@rem Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
@rem
@rem This software is provided 'as-is', without any express or implied
@rem warranty. In no event will the authors be held liable for any damages
@rem arising from the use of this software.
@rem
@rem Permission is granted to anyone to use this software for any purpose,
@rem including commercial applications, and to alter it and redistribute
@rem it freely, subject to the following restrictions:
@rem
@rem 1. The origin of this software must not be misrepresented; you must not
@rem    claim that you wrote the original software. If you use this software
@rem    in a product, an acknowledgment in the product documentation would
@rem    be appreciated but is not required.
@rem
@rem 2. Altered source versions must be plainly marked as such, and must not
@rem    be misrepresented as being the original software.
@rem
@rem 3. This notice may not be removed or altered from any source distribution.

@call %~dp0build\sbclc "build\make-script.lisp" %*