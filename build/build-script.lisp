;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(asdf:load-asd (merge-pathnames "build.asd"))
(asdf:load-system :build)

(defparameter *me* (pathname-name *load-truename*))

(let* ((image (first *posix-argv*))
       (args (rest *posix-argv*))
       (core *core-pathname*)
       (loaded-systems ())
       (loaded-files ())
       (executable nil)
       (gui nil)
       (toplevel-symbol-name "MAIN")
       (toplevel-package-name "COMMON-LISP-USER")
       (output-path (if executable "a.exe" "a.core"))
       (debug-build nil))
  (let ((unrecognized-args ())
        (additional-systems ()))
    (loop
      :for arg in args
      ;;Log output
      :when (cl-ppcre:scan "^-log" arg)
        :do (setf *standard-output* (open (subseq arg 4)
                                          :direction :output
                                          :if-exists :supersede)
                  *error-output* *standard-output*)
            ;;Search for any additional systems to load
      :else :when (cl-ppcre:scan "^-l" arg)
              :do (push (intern (string-upcase (subseq arg 2)) :keyword)
                        additional-systems)
                  ;;Search for any files to load on the target
      :else :when (cl-ppcre:scan "^-L" arg)
              :do (push (build:resolve-relative (subseq arg 2))
                        loaded-files)
                  ;;Check if should be executable (core otherwise)
      :else :when (cl-ppcre:scan "^-e" arg)
              :do (setf executable t)
                  ;;Check for debug build (nil otherwise)
      :else :when (cl-ppcre:scan "^-d" arg)
              :do (setf debug-build t)
                  ;;Check for toplevel entry function
      :else :when (cl-ppcre:scan "^-t" arg)
              :do
                 (cl-ppcre:register-groups-bind
                  (package name)
                  ("^(.*?)::?(.*)$" (subseq arg 2))
                  (setf toplevel-symbol-name (and name (string-upcase name)))
                  (setf toplevel-package-name (and package (string-upcase package))))
                 ;;Check if is gui app (console otherwise)
      :else :when (cl-ppcre:scan "^-g" arg)
              :do (setf gui t)
                  ;;Override input core path
      :else :when (cl-ppcre:scan "^-i" arg)
              :do (setf core
                        (build:resolve-relative (subseq arg 2)))
                  ;;Set the output core file
      :else :when (cl-ppcre:scan "^-o" arg)
              :do (setf output-path
                        (build:resolve-relative
                         (merge-pathnames
                          (subseq arg 2)
                          output-path)))
      :else :do (push arg unrecognized-args))

    (setf loaded-systems
          (nconc
           loaded-systems
           (nreverse additional-systems)))

    (setf loaded-files (nreverse loaded-files))

    (setf output-path
          (namestring
           (merge-pathnames
            output-path
            (make-pathname
             :type (if executable
                       "exe"
                       "core")))))

    (when unrecognized-args
      (setf unrecognized-args (nreverse unrecognized-args))
      (format t "~A: unrecognized arg~P:~{~&  ~A~}"
              *me*
              (length unrecognized-args)
              unrecognized-args)
      (finish-output)
      (exit :code 1)))

  (exit
   :code
   (build:build
    :image image
    :core core
    :loaded-systems loaded-systems
    :loaded-files loaded-files
    :executable executable
    :gui gui
    :toplevel-symbol-name toplevel-symbol-name
    :toplevel-package-name toplevel-package-name
    :output-path output-path
    :debug-build debug-build)))