;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(ql:quickload :cl-fad :silent t)
(ql:quickload :cl-ppcre :silent t)
(ql:quickload :cl-interpol :silent t)
(ql:quickload :command-line-arguments :silent t)
(ql:quickload :split-sequence :silent t)

(ql:quickload :build :silent t)

(cl-interpol:enable-interpol-syntax)

(defparameter *me* (pathname-name *load-truename*))

(defparameter *out-dir* #P"out/")
(defparameter *debug-build* nil)
(defparameter *version-number* "0.0.0")

(defun build-escaped-path (part &rest more-parts)
  (format
   nil
   "~{~A~^\\\\~}"
   (mapcan
    (lambda (part)
      (split-sequence:split-sequence
       #\\
       (substitute #\\ #\/ (namestring part))
       :remove-empty-subseqs t))
    (cons part more-parts))))

(defun sol ()
  (format t "make: building sol~%~%")

  (let ((code
         (build:build
          :loaded-systems '(:sol)
          :toplevel-package-name "SOL"
          :output-path (build:resolve-relative "sol/sol.exe" *out-dir*)
          :executable t
          :gui (not *debug-build*)
          :debug-build *debug-build*)))
    (cond
      ((zerop code)
       (format t "~%make: build successful~%~%")
       0)
      (t
       (format *error-output* #?"make: error in build: ${code}")
       code))))

(defun version-sol ()
  ;;NOTE: Can't do this. Messes up something with SBCL where it won't
  ;;      properly call the saved toplevel function, and seems to nuke
  ;;      all loaded code, period
  (format t "make: versioning sol~%~%")

  (let ((sol.exe
         (build-escaped-path
          (build:resolve-relative "sol/sol.exe" *out-dir*)))
        (version *version-number*)
        (description "sol")
        (product-name "sol")
        (product-version *version-number*)
        (copyright "Copyright (c) 2017 Wilfredo Velázquez-Rodríguez")
        (company "Wilfredo Velázquez-Rodríguez")
        (internal-name "sol"))
    (let ((code
           (process-exit-code
            (run-program
             "verpatch"
             (list sol.exe version "/va"
                   "/s" "description" description
                   "/s" "product" product-name
                   "/pv" product-version
                   "/s" "copyright" copyright
                   "/s" "company" company
                   "/s" "title" internal-name)
             :search t
             :input t
             :output t
             :error t))))
    (cond
      ((zerop code)
       (format t "make: versioning sol successful~%")
       0)
      (t
       (format *error-output* #?"make: error with versioning: ${code}~%")
       code)))))

(defun resource-copy ()
  (format t "make: copying resources~%")

  (let* ((out
          (build-escaped-path
           *out-dir*
           "sol/resources"))
         (code
          (process-exit-code
           (run-program
            "robocopy"
            (list "resources" out "/E" "/XO")
            :search t
            :input t
            :output t
            :error t))))
    ;;See https://ss64.com/nt/robocopy-exit.html for exit codes
    ;;notably, 1 is "everything went okay"
    (cond
      ((or (= code 0)
           (= code 1))
       (format t "make: copying successful~%")
       0)
      (t
       (format *error-output* #?"make: error in copy: ${code}~%")
       2))))

(defun dll-copy ()
  (let ((output-dir (truename (build:resolve-relative "sol/" *out-dir*))))
    (format t #?"make: copying dependent libraries to ${output-dir}~%~%")

    (dolist (d (mapcar
                (lambda (d)
                  (build:resolve-relative "bin/Windows/x64/" d))
                (cl-fad:list-directory "3rdparty/")))
      (dolist (bin-file (cl-fad:list-directory d))
        (format t #?"make: copying ${bin-file}~%")
        (cl-fad:copy-file
         bin-file
         (merge-pathnames
          (make-pathname
           :name (pathname-name bin-file)
           :type (pathname-type bin-file))
          output-dir)
         :overwrite t)))
    (format t  "~%make: finished copying files~%")
    0))

(defun installer ()
  (format t "make: building installer~%")

  (let* ((source-dir
          (build-escaped-path
           *out-dir*))
         (out
          (build-escaped-path
           *out-dir*
           "installer"))
         (version *version-number*)
         (code
          (process-exit-code
           (run-program
            "ISCC"
            (list
             #?"/O${out}" "build\\\\sol.iss"
             #?"/DSourceDir=${source-dir}"
             #?"/DVersion=${version}")
            :search t
            :input t
            :output t
            :error t))))
    (cond
      ((zerop code)
       (format t "~%make: finished building installer~%")
       0)
      (t
       (format *error-output* #?"make: error in installer build: ${code}~%")
       4))))

(defun main (args)
  (let ((targets '(sol
                   dll-copy
                   resource-copy
                   installer)))
    (let ((unrecognized-args ()))
      (loop
         :for arg in (cdr args)
         ;;Debug build
         :when (cl-ppcre:scan "^-d" arg)
         :do (setf *debug-build* t)
         ;;Output directory
         :else :when (cl-ppcre:scan "^-o" arg)
         :do (setf *out-dir* (cl-fad:pathname-as-directory (subseq arg 2)))
         :else :when (cl-ppcre:scan "^-v" arg)
         :do (let* ((ver-string (subseq arg 2)))
               (unless (cl-ppcre:scan "^(?:(\\d+)\\.)?(?:(\\d+)\\.)?(\\d+)$" ver-string)
                 (format *error-output*
                         "make: invalid version number '~A'" ver-string)
                 (return-from main 1))
               (destructuring-bind (major &optional (minor 0) (patch 0))
                   (mapcar #'parse-integer (split-sequence:split-sequence #\. ver-string))
                 (unless (<= 0 major 255)
                   (format *error-output*
                           "make: major version must be between 0 and 255~%")
                   (return-from main 1))
                 (unless (or (null minor)
                             (<= 0 minor 255))
                   (format *error-output*
                           "make: minor version must be between 0 and 255~%")
                   (return-from main 1))
                 (unless (or (null patch)
                             (<= 0 patch 65535))
                   (format *error-output*
                           "make: patch version must be between 0 and 65535~%")
                   (return-from main 1))

                 (setf *version-number* (format nil "~A.~A.~A" major minor patch))))
         :else :do (push arg unrecognized-args))

      (when unrecognized-args
        (setf unrecognized-args (nreverse unrecognized-args))
        (format *error-output*
                "make: unrecognized arg~P:~{~&  ~A~}"
                (length unrecognized-args)
                unrecognized-args)
        (return-from main 1)))
    ;;Normalize out-dir
    (setf *out-dir* (build:resolve-relative *out-dir*))
    (loop
       :for target :in targets
       :for code :=
       (handler-case
           (funcall target)
         (error (c)
           (format *error-output* "~%make: error while executing target '~A':~%~A~%" target c)
           -1))
       :when (not (zerop code))
       :return code
       :finally
       (return 0))))

(exit :code (main *posix-argv*))

(cl-interpol:disable-interpol-syntax)