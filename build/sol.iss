;;;Copyright (c) 2017 Wilfredo Vel�zquez-Rodr�guez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

#define AppName "sol"

#ifndef Version
#define Version "0.0.0"
#endif

#ifndef SourceDir
#define SourceDir "..\out\"
#endif

[Setup]
AppId={{70403B94-F29A-4DB4-9F69-98E59F711AF4}
AppName={#AppName}
AppVersion={#Version}
AppVerName={cm:NameAndVersion,{#AppName},{#Version}}
DefaultDirName={localappdata}\{#AppName}
DefaultGroupName={#AppName}
DisableProgramGroupPage=yes
OutputDir=installer\
OutputBaseFilename=setup_{#AppName}-{#Version}
Compression=lzma
SolidCompression=yes
UninstallFilesDir={app}\unins000
VersionInfoVersion={#Version}
UninstallDisplayName={#AppName}
AppCopyright=Copyright (c) 2017 Wilfredo Vel�zquez-Rodr�guez
PrivilegesRequired=lowest
VersionInfoCopyright=Copyright (c) 2017 Wilfredo Vel�zquez-Rodr�guez
VersionInfoProductName=sol
VersionInfoProductVersion={#Version}
VersionInfoProductTextVersion={#Version}
SourceDir={#SourceDir}
ArchitecturesAllowed=x64

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
; The main application files
Source: "sol\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\{cm:UninstallProgram,{#AppName}}"; Filename: "{uninstallexe}"
Name: "{group}\SOL"; Filename: "{app}\sol.exe"