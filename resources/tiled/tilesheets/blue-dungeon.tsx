<?xml version="1.0" encoding="UTF-8"?>
<tileset name="blue-dungeon" tilewidth="32" tileheight="32" tilecount="240" columns="24">
 <image source="blue-dungeon.png" trans="489551" width="768" height="320"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="6" height="32"/>
   <object id="2" x="6" y="0" width="26" height="8"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="32" height="8"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="30" height="8"/>
   <object id="2" x="24" y="8" width="6" height="24"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" x="8" y="16" width="24" height="16"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index">
   <object id="1" x="0" y="22" width="32" height="10"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="24" height="16"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" x="8" y="8" width="24" height="24"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="24" height="24"/>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="6" height="32"/>
  </objectgroup>
 </tile>
 <tile id="26">
  <objectgroup draworder="index">
   <object id="2" x="24" y="0" width="6" height="32"/>
  </objectgroup>
 </tile>
 <tile id="29">
  <objectgroup draworder="index">
   <object id="1" x="12" y="0" width="20" height="32"/>
  </objectgroup>
 </tile>
 <tile id="30">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="32" height="28"/>
  </objectgroup>
 </tile>
 <tile id="31">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="20" height="32"/>
  </objectgroup>
 </tile>
 <tile id="32">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="12" height="32"/>
  </objectgroup>
 </tile>
 <tile id="33">
  <objectgroup draworder="index">
   <object id="1" x="12" y="0" width="12" height="32"/>
  </objectgroup>
 </tile>
 <tile id="48">
  <objectgroup draworder="index">
   <object id="2" x="0" y="24" width="32" height="8"/>
   <object id="5" x="0" y="0" width="6" height="24"/>
  </objectgroup>
 </tile>
 <tile id="49">
  <objectgroup draworder="index">
   <object id="2" x="0" y="24" width="32" height="8"/>
  </objectgroup>
 </tile>
 <tile id="50">
  <objectgroup draworder="index">
   <object id="1" x="0" y="24" width="30" height="8"/>
   <object id="2" x="24" y="0" width="6" height="24"/>
  </objectgroup>
 </tile>
 <tile id="53">
  <objectgroup draworder="index">
   <object id="1" x="12" y="0" width="6" height="32"/>
  </objectgroup>
 </tile>
 <tile id="55">
  <objectgroup draworder="index">
   <object id="1" x="14" y="0" width="6" height="32"/>
  </objectgroup>
 </tile>
 <tile id="56">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="24" height="32"/>
  </objectgroup>
 </tile>
 <tile id="57">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="24" height="32"/>
  </objectgroup>
 </tile>
 <tile id="72">
  <objectgroup draworder="index">
   <object id="1" x="24" y="24" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="73">
  <objectgroup draworder="index">
   <object id="1" x="0" y="24" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="77">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="24" height="22"/>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="32" height="6"/>
  </objectgroup>
 </tile>
 <tile id="79">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="24" height="22"/>
  </objectgroup>
 </tile>
 <tile id="80">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="24" height="30"/>
  </objectgroup>
 </tile>
 <tile id="81">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="24" height="30"/>
  </objectgroup>
 </tile>
 <tile id="96">
  <objectgroup draworder="index">
   <object id="1" x="24" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="97">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="8"/>
  </objectgroup>
 </tile>
</tileset>
