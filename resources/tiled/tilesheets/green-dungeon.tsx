<?xml version="1.0" encoding="UTF-8"?>
<tileset name="green-dungeon" tilewidth="32" tileheight="32" tilecount="247" columns="19">
 <image source="green-dungeon.png" width="608" height="416"/>
 <terraintypes>
  <terrain name="Tile" tile="0"/>
 </terraintypes>
 <tile id="6" terrain="0,0,0,0" probability="100"/>
 <tile id="7" probability="2"/>
 <tile id="8" probability="2"/>
 <tile id="10" probability="2"/>
 <tile id="12" probability="0.25"/>
 <tile id="28" probability="0.5"/>
 <tile id="29" probability="0.5"/>
 <tile id="30" probability="0.25"/>
 <tile id="31" probability="0.5"/>
 <tile id="44" probability="0.25"/>
 <tile id="45" probability="0.25"/>
 <tile id="47" probability="0.25"/>
 <tile id="48" probability="0.25"/>
 <tile id="49" probability="0.25"/>
 <tile id="63" terrain="0,0,0,0" probability="2"/>
 <tile id="64" terrain="0,0,0,0" probability="2"/>
 <tile id="65" terrain="0,0,0,0" probability="2"/>
 <tile id="66" terrain="0,0,0,0" probability="2"/>
 <tile id="67" probability="0.25"/>
 <tile id="68" probability="0.25"/>
 <tile id="88" probability="0.5"/>
 <tile id="106" probability="0.25"/>
 <tile id="107" probability="0.5"/>
</tileset>
