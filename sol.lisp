;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol)

(defun run (&optional
              (working-dir
               (truename (asdf:system-relative-pathname :sol ""))))
  (uiop:with-current-directory (working-dir)
    (setf core:*sol-context*
          (make-instance
           'core:sol
           :load-plugins
           (list
            'sol.plugin.client:client-plugin)))
    (unwind-protect
         (progn
           (core:sol-init core:*sol-context*)
           (unwind-protect
                (progn
                  (sol.plugin.client::%enter-main-menu
                   (core:get-plugin 'sol.plugin.client:client-plugin))
                  (core:sol-run core:*sol-context*))
             (core:sol-uninit core:*sol-context*)))
      (setf core:*sol-context* nil))))

(defun run-async ()
  (bordeaux-threads:make-thread
   #'run))

(defun main (&rest argv)
  (declare (ignore argv))
  (run (truename "")))
