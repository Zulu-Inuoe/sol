;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defpackage #:sol.plugin.audio
  (:use #:alexandria #:cl #:sol.core)
  (:export
   #:audio-plugin))

(in-package #:sol.plugin.audio)

(defclass audio-plugin ()
  ((mixer-sound-chunks
    :initform nil
    :accessor mixer-sound-chunks)
   (mixer-music-chunks
    :initform nil
    :accessor mixer-music-chunks)))

(defmethod plugin-init ((audio audio-plugin))
  (let ((init-flags :mp3)
        (frequency 44100)
        (format :s16sys)
        (channels 2)
        (chunk-size 1028))
    (sdl2-mixer:init init-flags)

    (sdl2-mixer:open-audio frequency format channels chunk-size))
  (let ((background-music
         (sdl2-mixer:load-music "resources/music/dungeon.midi")))
    (push background-music (mixer-music-chunks audio))
    (sdl2-mixer:play-music background-music -1)))

(defmethod plugin-uninit ((audio audio-plugin))
  (sdl2-mixer:halt-music)
  (sdl2-mixer:close-audio)

  (dolist (music (mixer-music-chunks audio))
    (sdl2-mixer:free-music music))
  (setf (mixer-music-chunks audio) nil)

  (dolist (chunk (mixer-sound-chunks audio))
    (sdl2-mixer:free-chunk chunk))
  (setf (mixer-sound-chunks audio) nil)

  (sdl2-mixer:quit))
