;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass open-scene-dialog (ui:content-control)
  ((file-browser
    :type file-browser
    :reader %file-browser)
   (file-textbox
    :type ui:textbox
    :reader %file-textbox)
   (on-cancel-fn
    :type (or null function)
    :initarg :on-cancel
    :reader %on-cancel-fn)
   (on-open-fn
    :type (or null function)
    :initarg :on-open
    :reader %on-open-fn))
  (:default-initargs
   :background +shade-1+
    :horizontal-content-alignment :stretch
    :vertical-content-alignment :stretch))

(defmethod initialize-instance :after ((comp open-scene-dialog) &key)
  (let* ((file-browser
          (make-instance
           'file-browser
           :margin (make-instance 'ui:margin :left 5 :top 5 :right 5 :bottom 5)
              :dir "resources/scenes/"
              :pattern "*.lsc"
              :background +shade-3+))
         (file-textbox
          (make-instance
           'ui:textbox
           :vertical-alignment :center
           :margin (make-instance 'ui:margin :left 5 :right 5)
           :font-size 16))
         (open-command
          (make-instance
           'ui:delegate-command
           :execute
           (lambda (args)
             (declare (ignore args))
             (%open-scene-dialog-on-open comp))
           :can-execute
           (lambda (args)
             (declare (ignore args))
             (%open-scene-dialog-can-open comp)))))
    (setf (slot-value comp 'file-browser) file-browser)
    (setf (slot-value comp 'file-textbox) file-textbox)
    (event:event-subscribe
     (e_selected-item-changed file-browser)
     comp
     (lambda (comp args)
       (declare (ignore args))
       (setf (ui:text file-textbox)
             (enough-namestring (file-browser.selected-item file-browser)
                                (file-browser-dir file-browser)))
       (event:event-notify
        (ui:e_can-execute-changed open-command)
        (open-scene-dialog-selected-filename comp))))
    (event:event-subscribe
     (ui:e_text-changed file-textbox)
     comp
     (lambda (comp args)
       (declare (ignore args))
       (event:event-notify
        (ui:e_can-execute-changed open-command)
        (open-scene-dialog-selected-filename comp))))

    (setf (ui:content comp)
          (make-instance
           'ui:dock-panel
           :children
           (list
            (make-instance
             'ui:dock-panel
             :dock-panel.dock :bottom
             :margin (make-instance 'ui:margin :left 5 :top 5 :right 5 :bottom 5)
             :vertical-alignment :bottom
             :children
             (list
              (make-instance
               'ui:label
               :vertical-alignment :center
               :foreground media:*white* :font-size 16 :text "File:")
              (make-instance
               'ui:stack-panel
               :dock-panel.dock :right
               :vertical-alignment :center
               :orientation :horizontal
               :spacing 5
               :children
               (list
                (make-instance
                 'ui:button
                 :width 75 :height 40
                 :background +shade-2+
                 :clicked-color +shade-3+
                 :content "Cancel"
                 :command
                 (make-instance
                  'ui:delegate-command
                  :execute
                  (lambda (args)
                    (declare (ignore args))
                    (%open-scene-dialog-on-cancel comp))))
                (make-instance
                 'ui:button
                 :width 75 :height 40
                 :background +shade-2+
                 :clicked-color +shade-3+
                 :content "Open"
                 :command open-command)))
              file-textbox))
            file-browser)))))

(defun open-scene-dialog-selected-filename (comp)
  (if-let ((file-name
            (and
             (/= (length (ui:text (%file-textbox comp))) 0)
             (ui:text (%file-textbox comp)))))
    (merge-pathnames
     (make-pathname :type "lsc")
     file-name)
    nil))

(defun %open-scene-dialog-on-cancel (comp)
  (when-let ((fn (%on-cancel-fn comp)))
    (funcall fn)))

(defun %open-scene-dialog-can-open (comp)
  (/= (length (ui:text (%file-textbox comp)))
      0))

(defun %open-scene-dialog-on-open (comp)
  (when-let ((fn (%on-open-fn comp)))
    (funcall fn (merge-pathnames
                 (open-scene-dialog-selected-filename comp)
                 (file-browser-dir (%file-browser comp))))))
