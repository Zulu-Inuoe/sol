;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defsystem #:sol.plugin.client
  :name "sol.plugin.client"
  :version "0.0.0.0"
  :description "SOL Client Plugin"
  :author "Wilfredo Velázquez-Rodríguez <zulu.inuoe@gmail.com>"
  :license "zlib/libpng License <http://opensource.org/licenses/zlib-license.php>"
  :components
  ((:file "package")
   (:file "debug" :depends-on ("package"))
   (:file "fps-calculator" :depends-on ("package"))
   (:file "scene" :depends-on ("package" "debug"))
   (:file "player-script" :depends-on ("package" "debug"))
   (:file "pace-script" :depends-on ("package" "debug"))
   (:file "offline-gameplay" :depends-on ("package" "debug" "player-script" "pace-script"))
   (:file "file-browser-item" :depends-on ("package"))
   (:file "file-browser" :depends-on ("package" "file-browser-item"))
   (:file "open-scene-dialog" :depends-on ("package" "file-browser" "client-plugin"))
   (:file "client-plugin" :depends-on ("package" "debug" "offline-gameplay" "file-browser"))
   (:file "main-menu" :depends-on ("package" "client-plugin"))
   (:file "editor" :depends-on ("package" "client-plugin")))
  :depends-on
  (#:alexandria
   #:binary-types
   #:sdl2
   #:sol.core
   #:sol.components
   #:sol.ecs
   #:sol.event
   #:sol.game
   #:sol.idl
   #:sol.input
   #:sol.math
   #:sol.media
   #:sol.net
   #:sol.system.input
   #:sol.system.physics
   #:sol.system.render
   #:sol.ui))
