;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass %debug-text-entry ()
  ((text
    :initarg :text
    :accessor text)
   (x
    :initarg :x
    :accessor x)
   (y
    :initarg :y
    :accessor y)
   (initial-time
    :initarg :time
    :reader initial-time)
   (remaining-time
    :initarg :time
    :accessor remaining-time)))

(defun debug-init ()
  (event:event-subscribe
   (e_time-tick *sol-context*)
   nil
   '%on-debug-time-tick))

(defparameter *%tracked-debug-text* (make-hash-table))
(defparameter *%untracked-debug-text* nil)

(defun debug-render (renderer)
  (dolist (entry (hash-table-values *%tracked-debug-text*))
    (media:render-draw-text renderer (text entry) (x entry) (y entry)))
  (dolist (entry *%untracked-debug-text*)
    (media:render-draw-text renderer (text entry) (x entry) (y entry))))

(defun debug-uninit ()
  (setf *%untracked-debug-text* nil)
  (clrhash *%tracked-debug-text*)

  (event:event-unsubscribe
   (e_time-tick *sol-context*)
   '%on-debug-time-tick))

(defun debug-text (text x y &key (token nil) (time 5))
  (if token
      (%ensure-entry token text x y time)
      (setf *%untracked-debug-text*
            (nconc *%untracked-debug-text*
                   (list (%create-entry text x y time))))))

(defparameter *%debug-font*
  (make-instance
   'media:font
   :size 20
   :family "Segoe UI"))
(defparameter *%debug-color* media:*red*)


(defun %entry-expired (entry)
  (<= (remaining-time entry) 0))

(defun %on-debug-time-tick (dt)
  (flet ((update-entry (entry)
           (decf (remaining-time entry) dt)))
    (maphash-values #'update-entry *%tracked-debug-text*)
    (mapc #'update-entry *%untracked-debug-text*))
  (let ((removed-tracked
          (remove-if-not
           (lambda (cons)
             (%entry-expired (cdr cons)))
           (hash-table-alist *%tracked-debug-text*))))
    (dolist (cons removed-tracked)
      (media:dispose (text (cdr cons)))
      (remhash (car cons) *%tracked-debug-text*)))

  (let ((removed-untracked
         (remove-if-not #'%entry-expired *%untracked-debug-text*)))
    (dolist (entry removed-untracked)
      (media:dispose (text entry)))
    (setf *%untracked-debug-text* (delete-if #'%entry-expired *%untracked-debug-text*))))

(defun %create-entry (text x y time)
  (let ((entry
         (make-instance
          '%debug-text-entry
          :text (make-instance
                 'media:text
                 :color *%debug-color*
                 :font *%debug-font*
                 :string text)
          :x x
          :y y
          :time time)))
    entry))

(defun %ensure-entry (token text x y time)
  (let ((entry (gethash token *%tracked-debug-text*)))
    (cond
      ((null entry)
       (setf entry (%create-entry text x y time))
       (setf (gethash token *%tracked-debug-text*) entry))
      (t
       (setf (media:text-string (text entry))  text)
       (setf (x entry) x)
       (setf (y entry) y)
       (setf (remaining-time entry) time)))
    entry))
