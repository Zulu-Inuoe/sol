;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass file-browser-item (ui:content-control)
  ((filename
    :type pathname
    :initarg :filename
    :initform (error "file-browser-item: must supply filename")
    :reader filename)
   (is-selected
    :type boolean
    :initform nil
    :reader %file-browser-item-is-selected)
   (selected-color
    :type media:color
    :initarg :selected-color
    :initform media:*blue*
    :reader %file-browser-item-selected-color))
  (:default-initargs))

(defmethod initialize-instance :after ((comp file-browser-item) &key)
  (setf (ui:content comp)
        (make-instance
         'ui:label
         :font-size 16
         :text (namestring (filename comp)))))

(defun (setf %file-browser-item-is-selected) (value comp)
  (setf (slot-value comp 'is-selected) value)

  (if value
      (setf (ui:background (ui:content comp)) (%file-browser-item-selected-color comp))
      (setf (ui:background (ui:content comp)) media:*transparent*))

  value)

(defun (setf %file-browser-item-selected-color) (value comp)
  (setf (slot-value comp 'selected-color) value)

  (when (%file-browser-item-is-selected comp)
    (setf (ui:background (ui:content comp)) (%file-browser-item-selected-color comp)))

  value)
