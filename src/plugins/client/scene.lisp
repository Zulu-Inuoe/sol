;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass scene (serial:serializable)
  ((name
    :type string
    :initarg :name
    :initform (error "scene: must supply name")
    :accessor name)
   (entities.components
    :type list
    :initform nil
    :accessor entities.components)
   (event-manager
    :type ecs:event-manager
    :initform (make-instance 'ecs:event-manager)
    :reader event-manager)
   (entity-manager
    :type ecs:entity-manager
    :reader entity-manager)
   (system-manager
    :type ecs:system-manager
    :initform (make-instance 'ecs:system-manager)
    :reader system-manager)))

(defmethod serial:serialized-slots ((scene scene))
  '(name entities.components))

(defmethod serial:initialize-deserialized-instance ((scene scene))
  (setf (slot-value scene 'entity-manager)
        (make-instance 'ecs:entity-manager :event-manager (event-manager scene))))

(defmethod initialize-instance :after ((scene scene) &key)
  (setf (slot-value scene 'entity-manager)
        (make-instance 'ecs:entity-manager :event-manager (event-manager scene))))

(defun load-scene (name
                   &aux
                     (filename (%make-scene-filename name)))
  (serial:deserialize-file filename))

(defun save-scene (scene
                   &aux
                     (filename (%make-scene-filename (name scene))))
  (serial:serialize-file scene filename)
  (values))

(defun scene-init (scene)
  (loop :for (entity . components) :in (entities.components scene)
     :do
     (ecs:add-entity (entity-manager scene) entity)
     (dolist (c components)
       (ecs:add-component entity c))))

(defun scene-snapshot (scene)
  (setf (entities.components scene) nil)
  (dolist (entity (entities (entity-manager scene)))
    (push (cons entity (list-components entity))
          (entities.components scene)))
  (setf (entities.components scene) (nreverse (entities.components scene))))

(defun scene-uninit (scene)
  (dolist (entity (map 'list #'identity (entities (entity-manager scene))))
    (ecs:destroy-entity entity)))

(defun %make-scene-filename (scene-name)
  (make-pathname
   :directory (list :relative "resources/scenes")
   :name scene-name
   :type "lsc"))
