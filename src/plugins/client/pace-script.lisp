;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(define-component pace-script (script)
  ((movement-speed
    :type real
    :initarg :movement-speed
    :initform 0
    :accessor movement-speed
    :property-p t)
   (p1
    :type vec3
    :initform (vec3)
    :initarg :p1
    :accessor p1
    :property-p t)
   (p2
    :type vec3
    :initform (vec3)
    :initarg :p2
    :accessor p2
    :property-p t)
   (toggle
    :type boolean
    :initform nil
    :accessor toggle
    :property-p t)))

(defmethod fixed-update ((script pace-script) dt)
  (ecs:with-components (script-entity script)
      ((transform transform)
       (bod rigid-body))

    (let* ((target (if (toggle script) (p2 script) (p1 script)))
           (offset (vec- target (location transform))))
      (when (<= (vec-mag offset) 10)
        (setf (toggle script) (not (toggle script)))
        (setf target (if (toggle script) (p2 script) (p1 script)))
        (setf offset (vec- target (location transform))))
      ;;Handle movement input
      (let ((movement-dir
             (vec-norm offset)))
        (setf (velocity bod) (vec* movement-dir (movement-speed script)))
        (unless (zerop (vec-mag (velocity bod)))
          (setf (rotation transform)
                (atan (vec-z (vec-norm (velocity bod))) (vec-x (vec-norm (velocity bod))))))))))

(defmethod update ((script pace-script))
  (ecs:with-components (script-entity script)
      ((transform transform)
       (bod rigid-body)
       (sprite sprite))

    (let* ((x (cos (rotation transform)))
           (y (sin (rotation transform)))
           (direction
            (if (>= (abs x) (abs y))
                (if (plusp x) :right :left)
                (if (plusp y) :down :up))))

      (setf (current-animation sprite)
            (ecase direction
              (:left :move-left)
              (:right :move-right)
              (:up :move-up)
              (:down :move-down))))))
