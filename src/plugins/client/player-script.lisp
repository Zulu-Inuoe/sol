;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(define-component strike (script)
  ((owner
    :initarg :owner
    :initform (error "strike: must supply owner")
    :reader owner
    :property-p t)
   (width
    :type real
    :initarg :width
    :initform 20
    :reader width
    :property-p t)
   (height
    :type real
    :initarg :height
    :initform 20
    :reader height
    :property-p t)
   (duration
    :type real
    :initarg :duration
    :initform 0.2
    :reader duration
    :property-p t)
   (delay
    :type real
    :initarg :delay
    :initform 0
    :reader delay
    :property-p t)
   (delay-elapsed
    :type boolean
    :initform nil
    :accessor delay-elapsed)
   (active-time
    :type real
    :initform 0
    :accessor active-time)
   (delay-time
    :type real
    :initform 0
    :accessor delay-time)))

(defmethod fixed-update ((script strike) dt)
  (cond
    ((and (not (delay-elapsed script))
          (>= (incf (delay-time script) dt)
              (delay script)))
     (setf (delay-elapsed script) t)
     (ecs:add-component
      (script-entity script)
      (make-instance
       'rect-collider
       :trigger-p t
       :width (width script)
       :height (height script))))
    ((and (delay-elapsed script)
          (>= (incf (active-time script) dt)
              (duration script)))
     (write-line "miss")
     (ecs:destroy-entity (script-entity script)))))

(defmethod on-trigger ((script strike) col)
  (unless (eq (ecs:component-entity col)
              (owner script))
    (write-line "hit!")
    (ecs:destroy-entity (script-entity script))))

(defun strike (script location delay duration width height)
  (let ((target (make-instance 'ecs:entity :name "Strike")))
    (ecs:add-entity (script-entity-manager script) target)
    (ecs:add-component
     target
     (make-instance 'transform :location location))
    (ecs:add-component
     target
     (make-instance
      'strike
      :owner (script-entity script)
      :width width
      :height height
      :delay delay
      :duration duration))))

(defun punch (script transform
              &aux
                (direction (%rotation-to-dir (rotation transform)))
                (location
                 (vec+ (location transform)
                       (ecase direction
                         (:left (vec3 :x -14))
                         (:right (vec3 :x 14))
                         (:up (vec3 :z -22))
                         (:down (vec3 :z 22)))))
                 (width
                  (ecase direction
                    ((:left :right) 10)
                    ((:up :down) 15)))
                 (height
                  (ecase direction
                    ((:left :right) 15)
                    ((:up :down) 10))))
  (strike script location 0.3 0.1 width height))

(defun kick (script transform
              &aux
                (direction (%rotation-to-dir (rotation transform)))
                (location
                 (vec+ (location transform)
                       (ecase direction
                         (:left (vec3 :x -14 :z -5))
                         (:right (vec3 :x 14 :z -5))
                         (:up (vec3 :z -22))
                         (:down (vec3 :z 22)))))
                 (width
                  (ecase direction
                    ((:left :right) 10)
                    ((:up :down) 15)))
                 (height
                  (ecase direction
                    ((:left :right) 15)
                    ((:up :down) 10))))
  (strike script location 0.45 0.15 width height))

(defclass action-state ()
  ((name
    :type keyword
    :initarg :name
    :initform (error "action-state: must supply name")
    :reader name)
   (animation
    :type keyword
    :initarg :animation
    :initform (error "action-state: must supply animation")
    :reader animation)))

(defgeneric update-action-state (state script dt)
  (:method (state script dt)
    (declare (ignore state script dt))))

(defclass timed-state (action-state)
  ((duration
    :type real
    :initarg :duration
    :initform (error "timed-state: must supply duration")
    :reader duration)
   (elapsed-time
    :type real
    :initform 0
    :accessor elapsed-time)))

(defmethod update-action-state :before ((state timed-state) script dt)
  (declare (ignore script))
  (incf (elapsed-time state) dt))

(defclass idle (action-state)
  ()
  (:default-initargs
   :name :idle
    :animation :idle))

(defmethod update-action-state ((state idle) script dt)
  (ecs:with-components (script-entity script)
      ((states input-states)
       (transform transform)
       (bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (let ((backstep (get-input states :backstep))
          (r-weak (get-input states :r-weak))
          (r-strong (get-input states :r-strong))
          (l-weak (get-input states :l-weak))
          (l-strong (get-input states :l-strong))
          (movement-dir (movement-dir script)))
      ;;backstep
      (cond
        ((> (vec-mag movement-dir) 0)
         (setf (active-state script) (make-instance 'walk)))
        ((and
          (input-was-released backstep)
          (<= (input-time-since-pressed backstep) 0.25))
         (setf (active-state script)
               (make-instance
                'backstep
                :direction
                (vec-norm
                 (vec3
                  :x (- (cos (rotation transform)))
                  :z (- (sin (rotation transform))))))))
        ((and
          (input-was-pressed r-weak))
         (punch script transform)
         (setf (active-state script)
               (make-instance 'r-weak)))
        ((and
          (input-was-pressed r-strong))
         (kick script transform)
         (setf (active-state script)
               (make-instance 'r-strong)))
        ((and
          (input-was-pressed l-weak))
         (setf (active-state script)
               (make-instance 'l-weak)))
        ((and
          (input-was-pressed l-strong))
         (setf (active-state script)
               (make-instance 'l-strong)))))))

(defclass walk (action-state)
  ()
  (:default-initargs
   :name :walk
    :animation :move))

(defmethod update-action-state ((state walk) script dt)
  (ecs:with-components (script-entity script)
      ((states input-states)
       (transform transform)
       (bod rigid-body))
    (setf (velocity bod) (vec* (movement-dir script) (movement-speed script)))
    (unless (zerop (vec-mag (velocity bod)))
      (setf (rotation transform)
            (atan (vec-z (vec-norm (velocity bod))) (vec-x (vec-norm (velocity bod))))))

    (let ((run (get-input states :run))
          (dash (get-input states :dash))
          (roll (get-input states :roll))
          (r-weak (get-input states :r-weak))
          (r-strong (get-input states :r-strong))
          (l-weak (get-input states :l-weak))
          (l-strong (get-input states :l-strong))
          (movement-dir (movement-dir script)))
      (cond
        ((zerop (vec-mag movement-dir))
         (setf (active-state script) (make-instance 'idle)))
        ((and
          (input-is-held run)
          (>= (input-time-since-pressed run) 0.25)
          (<= (input-time-since-pressed run) 0.50))
         (setf (active-state script) (make-instance 'run)))
        ((and
          (input-was-released dash)
          (<= (input-time-since-pressed dash) 0.25))
         (setf (active-state script)
               (make-instance 'dash :direction movement-dir)))
        ((and
          (input-was-released roll)
          (<= (input-time-since-pressed roll) 0.25))
         (setf (active-state script)
               (make-instance 'roll :direction (vec-norm movement-dir))))
        ((and
          (input-was-pressed r-weak))
         (punch script transform)
         (setf (active-state script)
               (make-instance 'r-weak)))
        ((and
          (input-was-pressed r-strong))
         (kick script transform)
         (setf (active-state script)
               (make-instance 'r-strong)))
        ((and
          (input-was-pressed l-weak))
         (setf (active-state script)
               (make-instance 'l-weak)))
        ((and
          (input-was-pressed l-strong))
         (setf (active-state script)
               (make-instance 'l-strong)))))))

(defclass run (action-state)
  ()
  (:default-initargs
   :name :run
    :animation :run))

(defmethod update-action-state ((state run) script dt)
  (ecs:with-components (script-entity script)
      ((states input-states)
       (transform transform)
       (bod rigid-body))
    (setf (velocity bod) (vec* (movement-dir script) (movement-speed script) 2.5))
    (unless (zerop (vec-mag (velocity bod)))
      (setf (rotation transform)
            (atan (vec-z (vec-norm (velocity bod))) (vec-x (vec-norm (velocity bod))))))

    (let ((run (get-input states :run))
          (r-weak (get-input states :r-weak))
          (r-strong (get-input states :r-strong))
          (l-weak (get-input states :l-weak))
          (l-strong (get-input states :l-strong))
          (movement-dir (movement-dir script)))
      (cond
        ((zerop (vec-mag movement-dir))
         (setf (active-state script) (make-instance 'idle)))
        ((not (input-is-held run))
         (setf (active-state script) (make-instance 'walk)))
        ((and
          (input-was-pressed r-weak))
         (setf (active-state script)
               (make-instance 'r-weak)))
        ((and
          (input-was-pressed r-strong))
         (setf (active-state script)
               (make-instance 'r-strong)))
        ((and
          (input-was-pressed l-weak))
         (setf (active-state script)
               (make-instance 'l-weak)))
        ((and
          (input-was-pressed l-strong))
         (setf (active-state script)
               (make-instance 'l-strong)))))))

(defclass dash (timed-state)
  ((direction
    :type vec3
    :initarg :direction
    :initform (error "dash: must supply direction")
    :reader direction))
  (:default-initargs
   :name :dash
    :animation :move
    :duration 0.075))

(defmethod update-action-state ((state dash) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) (vec* (direction state) (movement-speed script) 4.0))
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass backstep (timed-state)
  ((direction
    :type vec3
    :initarg :direction
    :initform (error "backstep: must supply direction")
    :reader direction))
  (:default-initargs
   :name :backstep
    :animation :move
    :duration 0.125))

(defmethod update-action-state ((state backstep) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) (vec* (direction state) (movement-speed script) 3.0))
    (when (>= (elapsed-time state) (duration state))
      (setf (active-state script) (make-instance 'post-backstep)))))

(defclass post-backstep (timed-state)
  ()
  (:default-initargs
   :name :post-backstep
    :animation :post-backstep
    :duration 0.25))

(defmethod update-action-state ((state post-backstep) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass roll (timed-state)
  ((direction
    :type vec3
    :initarg :direction
    :initform (error "roll: must supply direction")
    :reader direction))
  (:default-initargs
   :name :roll
    :animation :roll
    :duration 0.45))

(defmethod update-action-state ((state roll) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) (vec* (direction state) (movement-speed script) 2.5))
    (when (>= (elapsed-time state) (duration state))
      (setf (active-state script) (make-instance 'post-roll)))))

(defclass post-roll (timed-state)
  ()
  (:default-initargs
   :name :post-roll
    :animation :post-roll
    :duration 0.25))

(defmethod update-action-state ((state post-roll) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass r-weak (timed-state)
  ()
  (:default-initargs
   :name :r-weak
    :animation :punch
    :duration 0.65))

(defmethod update-action-state ((state r-weak) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass r-strong (timed-state)
  ()
  (:default-initargs
   :name :r-strong
    :animation :kick
    :duration 0.65))

(defmethod update-action-state ((state r-strong) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass l-weak (timed-state)
  ()
  (:default-initargs
   :name :l-weak
    :animation :idle
    :duration 0.25))

(defmethod update-action-state ((state l-weak) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass l-strong (timed-state)
  ()
  (:default-initargs
   :name :l-strong
    :animation :idle
    :duration 0.25))

(defmethod update-action-state ((state l-strong) script dt)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(define-component player-script (script)
  ((movement-speed
    :type real
    :initarg :movement-speed
    :initform 0
    :accessor movement-speed
    :property-p t)
   (movement-dir
    :type vec3
    :initform +vec-zero+
    :accessor movement-dir
    :property-p t)
   (active-state
    :type action-state
    :initform (make-instance 'idle)
    :accessor active-state)))

(defmethod fixed-update ((script player-script) dt)
  (ecs:with-components (script-entity script)
      ((states input-states))
    (let ((left/right (get-input states :left/right))
          (up/down (get-input states :up/down))
          (left (get-input states :left))
          (up (get-input states :up))
          (right (get-input states :right))
          (down (get-input states :down)))

      ;;Handle movement input
      (with-slots (movement-dir) script
        (setf movement-dir +vec-zero+)
        (when (input-is-held left)
          (setf movement-dir
                (vec- movement-dir (vec* +vec-right+ (input-value left)))))

        (when (input-is-held up)
          (setf movement-dir
                (vec+ movement-dir (vec* +vec-forward+ (input-value up)))))

        (when (input-is-held right)
          (setf movement-dir
                (vec+ movement-dir (vec* +vec-right+ (input-value right)))))

        (when (input-is-held down)
          (setf movement-dir
                (vec- movement-dir (vec* +vec-forward+ (input-value down)))))

        (when (input-is-held left/right)
          (setf movement-dir
                (vec+ movement-dir (vec* +vec-right+ (input-value left/right)))))

        (when (input-is-held up/down)
          (setf movement-dir
                (vec- movement-dir (vec* +vec-forward+ (input-value up/down)))))

        (when (> (vec-mag movement-dir) 1)
          (setf movement-dir (vec-norm movement-dir))))))
  (update-action-state (active-state script) script dt))

(defmethod update ((script player-script))
  (ecs:with-components (script-entity script)
      ((transform transform)
       (bod rigid-body)
       (sprite sprite))

    (let* ((direction (%rotation-to-dir (rotation transform))))
      (debug-text
       (format nil "state: ~A" (name (active-state script)))
       5 510 :token :state)
      (debug-text
       (format nil "rotation: ~5,2,,,'0F" (rotation transform))
       5 530 :token :rotation)
      (debug-text
       (format nil "location: ~A" (location transform))
       5 550 :token :position)

      (let ((animation
             (make-keyword (format nil "~A-~A" (animation (active-state script)) direction))))
        (debug-text
         (format nil "animation: ~A" animation)
         5 570 :token :animation)
        (setf (current-animation sprite) animation)))))

(defmethod on-collision ((script player-script) col)
  (debug-text
   (format nil "Collided with: ~A" col)
   5 460 :token :collision :time 1))

(defmethod on-trigger ((script player-script) col)
  (debug-text
   (format nil "Triggered on: ~A" col)
   5 480 :token :trigger :time 1))

(defun %rotation-to-dir (rotation)
  (let ((x (cos rotation))
        (y (sin rotation)))
    (if (>= (abs x) (abs y))
        (if (plusp x) :right :left)
        (if (plusp y) :down :up))))
