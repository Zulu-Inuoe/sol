;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass editor (ui:content-control)
  ((client-plugin
    :type client-plugin
    :initarg :client-plugin
    :initform (error "editor: must provide a client-plugin")
    :reader client-plugin)
   (scene
    :type (or null scene)
    :initform nil
    :reader editor-scene)
   (overlay
    :type ui:content-control
    :reader %editor-overlay)
   (scene-label
    :type ui:label
    :reader %editor-scene-label)
   (exit-command
    :type ui:delegate-command
    :reader %editor-exit-command)
   (load-command
    :type ui:delegate-command
    :reader %editor-load-command)
   (save-command
    :type ui:delegate-command
    :reader %editor-save-command)
   (play-command
    :type ui:delegate-command
    :reader %editor-play-command)
   (stop-command
    :type ui:delegate-command
    :reader %editor-stop-command)
   (gameplay
    :type (or null gameplay)
    :initform nil
    :reader %editor-gameplay))
  (:default-initargs
   :horizontal-content-alignment :stretch
    :vertical-content-alignment :stretch))

(defmethod initialize-instance :after ((comp editor)
                                       &key &allow-other-keys
                                       &aux (plugin (client-plugin comp)))
  (let* ((scene-label (make-instance 'ui:label :foreground media:*white* :font-size 16 :text "<scene-name>"))
         (overlay
          (make-instance
           'ui:content-control
           :visible nil
           :horizontal-content-alignment :stretch
           :vertical-content-alignment :stretch))
         (exit-button
          (make-instance
           'ui:button
           :width 75 :height 40
           :background +shade-2+
           :clicked-color +shade-3+
           :content "Exit"
           :command
           (make-instance
            'ui:delegate-command
            :execute
            (lambda (args)
              (declare (ignore args))
              (event:event-once
               (e_frame-tick plugin)
               nil
               (lambda (args)
                 (declare (ignore args))
                 (%leave-editor plugin)
                 (%enter-main-menu plugin))))
            :can-execute
            (lambda (args)
              (declare (ignore args))
              (null (%editor-gameplay comp))))))
         (load-button
          (make-instance
           'ui:button
           :width 75 :height 40
           :background +shade-2+
           :clicked-color +shade-3+
           :content "Load"
           :command
           (make-instance
            'ui:delegate-command
            :execute
            (lambda (args)
              (declare (ignore args))
              (setf (ui:content overlay)
                    (make-instance
                     'open-scene-dialog
                     :on-open
                     (lambda (filename)
                       (setf (ui:content overlay) nil)
                       (setf (ui:visible overlay) nil)
                       (%editor-load comp filename))
                     :on-cancel
                     (lambda ()
                       (setf (ui:content overlay) nil)
                       (setf (ui:visible overlay) nil))))
              (setf (ui:visible overlay) t))
            :can-execute
            (lambda (args)
              (declare (ignore args))
              (null (%editor-gameplay comp))))))
         (save-button
          (make-instance
           'ui:button
           :width 75 :height 40
           :background +shade-2+
           :clicked-color +shade-3+
           :content "Save"
           :command
           (make-instance
            'ui:delegate-command
            :execute
            (lambda (args)
              (declare (ignore args))
              (%editor-save comp))
            :can-execute
            (lambda (args)
              (declare (ignore args))
              (and
               (editor-scene comp)
               (null (%editor-gameplay comp)))))))
         (play-button
          (make-instance
           'ui:button
           :width 75 :height 40
           :background +shade-2+
           :clicked-color +shade-3+
           :content "Play"
           :command
           (make-instance
            'ui:delegate-command
            :execute
            (lambda (args)
              (declare (ignore args))
              (%editor-play comp))
            :can-execute
            (lambda (args)
              (declare (ignore args))
              (and
               (editor-scene comp)
               (null (%editor-gameplay comp)))))))
         (stop-button
          (make-instance
           'ui:button
           :width 75 :height 40
           :background +shade-2+
           :clicked-color +shade-3+
           :content "Stop"
           :command
           (make-instance
            'ui:delegate-command
            :execute
            (lambda (args)
              (declare (ignore args))
              (%editor-stop comp))
            :can-execute
            (lambda (args)
              (declare (ignore args))
              (%editor-gameplay comp))))))
    (setf (slot-value comp 'overlay) overlay)
    (setf (slot-value comp 'scene-label) scene-label)
    (setf (slot-value comp 'exit-command) (ui:command exit-button))
    (setf (slot-value comp 'load-command) (ui:command load-button))
    (setf (slot-value comp 'save-command) (ui:command save-button))
    (setf (slot-value comp 'play-command) (ui:command play-button))
    (setf (slot-value comp 'stop-command) (ui:command stop-button))
    (setf (ui:content comp)
          (make-instance
           'ui:grid
           :children
           (list
            (make-instance
             'ui:stack-panel
             :margin (make-instance 'ui:margin :left 5 :top 5)
             :horizontal-alignment :left
             :vertical-alignment :top
             :background +shade-1+
             :orientation :vertical
             :spacing 5
             :children
             (list
              (make-instance
               'ui:stack-panel
               :margin (make-instance 'ui:margin :left 5 :top 5 :right 5)
               :orientation :horizontal
               :spacing 5
               :children
               (list
                exit-button
                load-button
                save-button
                play-button
                stop-button))
              (make-instance
               'ui:stack-panel
               :margin (make-instance 'ui:margin :left 5 :right 5)
               :orientation :horizontal
               :spacing 5
               :children
               (list
                (make-instance 'ui:label :foreground media:*white* :font-size 16 :text "Current Scene:")
                scene-label))))
            overlay)))))

(defun (setf editor-scene) (new-val editor
                            &aux (old-val (editor-scene editor)))
  (unless (eq new-val old-val)
    (setf (slot-value editor 'scene) new-val)
    (when new-val
      (setf (ui:text (%editor-scene-label editor)) (name new-val)))
    (event:event-notify
     (ui:e_can-execute-changed (%editor-save-command editor))
     nil)
    (event:event-notify
     (ui:e_can-execute-changed (%editor-play-command editor))
     nil))

  (editor-scene editor))

(defun %editor-load (editor filename)
  (if (uiop/filesystem:file-exists-p filename)
      (setf (editor-scene editor) (load-scene (pathname-name filename)))
      (setf (editor-scene editor) (make-instance 'scene :name (pathname-name filename))))
  (values))

(defun %editor-save (editor
                     &aux
                       (scene (editor-scene editor)))
  (save-scene scene)
  (values))

(defun %editor-play (editor
                     &aux
                       (scene (editor-scene editor))
                       (filename (%make-scene-filename (name scene))))
  (%editor-save editor)
  (setf (slot-value editor 'gameplay)
        (make-instance
         'gameplay
         :context (client-plugin editor)
         :initial-scene (serial:deserialize-file filename)))
  (gameplay-init (%editor-gameplay editor))

  (event:event-notify
   (ui:e_can-execute-changed (%editor-exit-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-load-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-save-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-play-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-stop-command editor))
   nil)

  (values))

(defun %editor-stop (editor)
  (gameplay-uninit (%editor-gameplay editor))
  (setf (slot-value editor 'gameplay) nil)

  (event:event-notify
   (ui:e_can-execute-changed (%editor-exit-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-load-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-save-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-play-command editor))
   nil)
  (event:event-notify
   (ui:e_can-execute-changed (%editor-stop-command editor))
   nil)

  (values))

(defun %make-test-scene (name)
  (let ((scene (make-instance 'scene :name name)))
    ;;A tiled map..
    (let ((map (make-instance 'ecs:entity :name "tiled-map")))
      (push
       (cons
        map
        (list
         (make-instance 'transform)
         (make-instance
          'tiled-map :file-path (uiop:ensure-absolute-pathname #p"resources/tiled/map.tmx" *default-pathname-defaults*))))
       (entities.components scene)))

    ;;Test entity
    (let ((target (make-instance 'ecs:entity :name "standing")))
      (push
       (cons
        target
        (list
         (make-instance
          'sprite
          :path "resources/images/metal-slug-mummy/metal-slug-mummy.lanim")
         (make-instance
          'transform :location (vec3 :x 300 :z 400))
         (make-instance
          'rect-collider
          :width 37
          :height 45)))
       (entities.components scene)))

    ;;Test entity (non-kinematic, moving)
    (let ((target (make-instance 'ecs:entity :name "pacer")))
      (push
       (cons
        target
        (list
         (make-instance
          'sprite
          :path "resources/images/metal-slug-mummy/metal-slug-mummy.lanim")
         (make-instance
          'transform :location (vec3 :x 100 :z 175))
         (make-instance
          'rigid-body)
         (make-instance
          'pace-script
          :movement-speed 40
          :p1 (vec3 :x 100 :z 175)
          :p2 (vec3 :x 250 :z 175))
         (make-instance
          'rect-collider
          :width 37
          :height 45)))
       (entities.components scene)))

    ;;Test entity (kinematic)
    (let ((target (make-instance 'ecs:entity :name "kinematic")))
      (push
       (cons
        target
        (list
         (make-instance
          'sprite
          :path "resources/images/metal-slug-mummy/metal-slug-mummy.lanim")
         (make-instance
          'transform :location (vec3 :x 550 :z 300))
         (make-instance
          'rigid-body
          :kinematic-p t)
         (make-instance
          'rect-collider
          :width 37
          :height 45)))
       (entities.components scene)))

    ;;Test trigger entity
    (let ((target (make-instance 'ecs:entity :name "trigger")))
      (push
       (cons
        target
        (list
         (make-instance
          'transform :location (vec3 :x 300 :z 300))
         (make-instance
          'rect-collider
          :trigger-p t
          :width 37
          :height 45)))
       (entities.components scene)))

    ;;Test trigger entity (moving)
    (let ((target (make-instance 'ecs:entity :name "moving trigger")))
      (push
       (cons
        target
        (list
         (make-instance
          'transform :location (vec3 :x 300 :z 500))
         (make-instance
          'rigid-body)
         (make-instance
          'pace-script
          :movement-speed 40
          :p1 (vec3 :x 300 :z 500)
          :p2 (vec3 :x 400 :z 500))
         (make-instance
          'rect-collider
          :trigger-p t
          :width 37
          :height 45)))
       (entities.components scene)))

    ;;Create the player character
    (let ((player (make-instance 'ecs:entity :name "player")))
      (push
       (cons
        player
        (list
         (make-instance 'camera :activep t)
         (make-instance
          'text
          :string "Hello, world!"
          :color media:*red*
          :offset-x -16
          :offset-y 10)
         ;;Give it a sprite and a transform
         (make-instance
          'sprite
          :path "resources/images/primm/primm.lanim")

         (make-instance 'transform :location (vec3 :x 100 :z 100))

         ;;Give it player input
         (make-instance
          'player-input
          :player-handle
          (make-instance
           'input:player-handle
           :devices nil
           :global t)
          :input-maps
          '((:keyboard/mouse
             (:scancode-a :left
              :scancode-left :left

              :scancode-d :right
              :scancode-right :right

              :scancode-w :up
              :scancode-up :up

              :scancode-s :down
              :scancode-down :down

              :scancode-lshift :run
              :scancode-rshift :run
              :scancode-lalt :dash
              :scancode-space :roll
              :scancode-space :backstep

              :mouse-button-middle :two-hand
              :mouse-button-left :r-weak
              :mouse-button-right :l-weak)
             :controller
             (:dpad-left :left
              :dpad-right :right
              :dpad-up :up
              :dpad-down :down
              :left-x :left/right
              :left-y :up/down
              :b :run
              :a :dash
              :a :backstep
              :b :backstep
              :b :roll
              :y :two-hand
              :left-shoulder :l-weak
              :left-trigger :l-strong
              :right-shoulder :r-weak
              :right-trigger :r-strong))))

         ;;A collider
         (make-instance
          'rect-collider
          :width 16 :height 32)

         ;;And a rigid-body
         (make-instance 'rigid-body)

         (make-instance
          'player-script
          :movement-speed 85)

         (make-instance
          'input-states
          :states
          (list
           ;;Movement Buttons
           :left :right :up :down
           ;;Movement Axes
           :left/right :up/down

           ;;Action Buttons
           :dash :backstep :roll :run
           :two-hand
           :r-weak :r-strong
           :l-weak :l-strong
           :menu))))
       (entities.components scene)))

    scene))
