;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass gameplay ()
  ((context
    :initarg :context
    :initform (error "gameplay: must supply context")
    :reader context)
   (initial-scene
    :type scene
    :initarg :initial-scene
    :initform (error "gameplay: must supply initial scene")
    :reader gameplay-initial-scene)
   (current-scene
    :type (or null scene)
    :initform nil
    :reader gameplay-current-scene)))

(defun (setf gameplay-current-scene) (new-val gameplay
                                      &aux
                                        (old-val (gameplay-current-scene gameplay)))
  (unless (eq new-val old-val)
    (setf (slot-value gameplay 'current-scene) new-val)
    (when old-val
      (%gameplay-unsetup-scene gameplay old-val))
    (when new-val
      (%gameplay-setup-scene gameplay new-val)))

  (gameplay-current-scene gameplay))

(defun gameplay-init (gameplay)
  (setf (gameplay-current-scene gameplay) (gameplay-initial-scene gameplay)))

(defun gameplay-uninit (gameplay)
  (setf (gameplay-current-scene gameplay) nil))

(defun %gameplay-setup-scene (gameplay scene)
  (let ((event-manager (event-manager scene))
        (system-manager (system-manager scene))
        (entity-manager (entity-manager scene)))
    (forward-event event-manager :time-tick (e_time-tick *sol-context*))
    (forward-event event-manager :frame-tick (e_frame-tick (context gameplay)))

    (add-system
     system-manager
     (make-instance
      'sol.system.input:input-system
      :entity-manager entity-manager
      :event-manager event-manager
      :input-manager (input:input-manager)))

    (add-system
     system-manager
     (make-instance
      'sol.system.render:render-system
      :entity-manager entity-manager
      :event-manager event-manager
      :renderer (ui:window-renderer (window (context gameplay)))))

    (add-system
     system-manager
     (make-instance
      'sol.system.physics:physics-system
      :entity-manager entity-manager
      :event-manager event-manager))

    (configure-systems system-manager)
    (scene-init scene))
  (values))

(defun %gameplay-unsetup-scene (gameplay scene)
  (scene-uninit scene)
  (unconfigure-systems (system-manager scene))
  (let ((event-manager (event-manager scene)))
    (unforward-event event-manager :frame-tick (e_frame-tick (context gameplay)))
    (unforward-event event-manager :time-tick (e_time-tick *sol-context*)))
  (values))
