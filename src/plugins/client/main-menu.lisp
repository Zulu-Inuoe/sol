;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass main-menu (ui:content-control)
  ((client-plugin
    :type client-plugin
    :initarg :client-plugin
    :initform (error "main-menu: must provide a client-plugin")
    :reader client-plugin))
  (:default-initargs
   :horizontal-content-alignment :stretch
    :vertical-content-alignment :stretch))

(defmethod initialize-instance :after ((comp main-menu)
                                       &key &allow-other-keys
                                       &aux (plugin (client-plugin comp)))
  (let ((banner-image
         (make-instance
          'ui:image
          :vertical-alignment :top
          :margin (make-instance 'ui:margin :top 50)
          :source "resources/images/banner/banner1.png"
          :stretch :none))
        (start-button
         (make-instance
          'ui:button
          :width 75 :height 40
          :content "Start"
          :background +shade-2+
          :clicked-color +shade-3+
          :command
          (make-instance
           'ui:delegate-command
           :execute (lambda (args)
                      (declare (ignore args))
                      (%leave-main-menu plugin)
                      (%enter-offline-game plugin)))))
        (editor-button
         (make-instance
          'ui:button
          :width 75 :height 40
          :content "Editor"
          :background +shade-2+
          :clicked-color +shade-3+
          :command
          (make-instance
           'ui:delegate-command
           :execute (lambda (args)
                      (declare (ignore args))
                      (%leave-main-menu plugin)
                      (%enter-editor plugin)))))
        (options-button
         (make-instance
          'ui:button
          :width 75 :height 40
          :background +shade-2+
          :clicked-color +shade-3+
          :content "Options"
          :is-enabled nil))
        (exit-button
         (make-instance
          'ui:button
          :width 75 :height 40
          :background +shade-2+
          :clicked-color +shade-3+
          :content "Exit"
          :command
          (make-instance
           'ui:delegate-command
           :execute (lambda (args)
                      (declare (ignore args))
                      (sol-exit *sol-context*))))))

    (setf (ui:content comp)
          (make-instance
           'ui:grid
           :background +shade-1+
           :children
           (list
            banner-image
            (make-instance
             'ui:stack-panel
             :orientation :vertical
             :spacing 15
             :horizontal-alignment :left
             :vertical-alignment :top
             :margin
             (make-instance 'ui:margin :left 50 :top 200)
             :children
             (list
              start-button
              editor-button
              options-button
              exit-button)))))))
