;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defparameter +shade-1+ (media:color :r 85 :g 43 :b 114))
(defparameter +shade-2+ (media:color :r 149 :g 117 :b 171))
(defparameter +shade-3+ (media:color :r 115 :g 76 :b 142))
(defparameter +shade-4+ (media:color :r 58 :g 19 :b 85))
(defparameter +shade-5+ (media:color :r 35 :g 3 :b 57))

(defclass client-plugin ()
  ((window
    :type ui:window
    :accessor window)
   ;;This is the currently active tree
   (ui-tree
    :initform nil
    :accessor ui-tree)
   (gameplay
    :initform nil
    :accessor gameplay)
   (e_frame-tick
    :type event:event
    :initform (make-instance 'event:event :name "frame-tick")
    :reader e_frame-tick)
   (%ui-host
    :type ui:content-control
    :accessor %ui-host)
   (%fps-calc
    :type fps-calculator
    :initform (make-instance 'fps-calculator)
    :reader %fps-calc)))

(defmethod ui-tree ((plugin client-plugin))
  (ui:content (%ui-host plugin)))

(defmethod (setf ui-tree) (value (plugin client-plugin))
  (setf (ui:content (%ui-host plugin)) value))

(defmethod initialize-instance :after ((plugin client-plugin) &key &allow-other-keys))

(defmethod plugin-init ((plugin client-plugin))
  (event:event-subscribe
   (e_time-tick *sol-context*)
   plugin
   '%on-time-tick)

  (event:event-subscribe
   (e_time-ticks-completed *sol-context*)
   plugin
   '%on-time-ticks-completed)

  ;;Init debug
  (debug-init)

  ;;Init media
  (media:media-init)

  ;;Init input manager
  (input:input-manager-init)

  ;;Init UI manager
  (ui:ui-manager-init)

  ;;Create our window
  (setf (window plugin)
        (make-instance
         'ui:window
         :title "sol"))

  (setf (%ui-host plugin)
        (make-instance
         'ui:content-control
         :horizontal-content-alignment :stretch
         :vertical-content-alignment :stretch))

  (setf (ui:content (window plugin))
        (make-instance
         'ui:grid
         :children
         (list
          (%ui-host plugin))))

  (event:event-subscribe
   (ui:e_window-closed (window plugin))
   plugin
   (lambda (plugin window)
     (declare (ignore plugin window))
     (sol-exit *sol-context*))))

(defmethod plugin-uninit ((plugin client-plugin))
  (event:event-unsubscribe
   (ui:e_window-closed (window plugin))
   plugin)
  (ui:window-close (window plugin))
  (slot-makunbound plugin '%ui-host)
  (slot-makunbound plugin 'window)

  (ui:ui-manager-uninit)
  (input:input-manager-uninit)

  (debug-uninit)

  (event:event-unsubscribe
   (e_time-tick *sol-context*)
   plugin)

  (event:event-unsubscribe
   (e_time-ticks-completed *sol-context*)
   plugin))

(defun %enter-main-menu (plugin)
  (setf (ui-tree plugin) (%make-main-menu-ui-tree plugin)))

(defun %leave-main-menu (plugin)
  (setf (ui-tree plugin) nil))

(defun %enter-offline-game (plugin)
  (setf (ui-tree plugin) (%make-offline-game-ui-tree plugin))

  (setf (gameplay plugin)
        (make-instance
         'gameplay
         :context plugin
         :initial-scene (%make-test-scene "test")))

  (gameplay-init (gameplay plugin)))

(defun %leave-offline-game (plugin)
  (gameplay-uninit (gameplay plugin))
  (setf (gameplay plugin) nil)

  (setf (ui-tree plugin) nil))

(defun %enter-editor (plugin)
  (setf (ui-tree plugin) (%make-editor-ui-tree plugin)))

(defun %leave-editor (plugin)
  (setf (ui-tree plugin) nil))

(defun %make-main-menu-ui-tree (plugin)
  (make-instance 'main-menu :client-plugin plugin))

(defun %make-offline-game-ui-tree (plugin)
  (let ((exit-button
         (make-instance
          'ui:button
          :horizontal-alignment :left
          :vertical-alignment :top
          :margin (make-instance 'ui:margin :left 10 :top 10)
          :width 75 :height 40
          :background +shade-2+
          :clicked-color +shade-3+
          :content "Exit"
          :command
          (make-instance
           'ui:delegate-command
           :execute
           (lambda (args)
             (declare (ignore args))
             (event:event-once
              (e_frame-tick plugin)
              nil
              (lambda (args)
                (declare (ignore args))
                (%leave-offline-game plugin)
                (%enter-main-menu plugin))))))))

    (make-instance
     'ui:grid
     :children
     (list
      exit-button))))

(defun %make-editor-ui-tree (plugin)
  (make-instance 'editor :client-plugin plugin))

(defun %on-time-tick (plugin dt)
  (fps-time-tick (%fps-calc plugin) dt)
  (values))

(defun %on-time-ticks-completed (plugin args
                                 &aux
                                   (renderer (ui:window-renderer (window plugin))))
  (fps-frame-tick (%fps-calc plugin))
  (debug-text (format nil "FPS: ~5,2,,,'0F" (float (fps (%fps-calc plugin)))) 700 570 :token :fps)
  ;;Clear the background
  (media:render-clear renderer :color media:*black*)

  (%draw-checkerboard renderer)

  ;;Notify a new frame tick
  ;;so that everyone renders
  (event:event-notify
   (e_frame-tick plugin)
   args)

  ;;Render the UI on top of that
  (ui:draw (window plugin))

  ;;Draw the debug on top of that
  (debug-render renderer)

  ;;And present
  (sdl2:render-present (media:renderer-native renderer)))

(defun %renderer-size (sdl-renderer)
  (cffi:with-foreign-objects ((w :int)
                              (h :int))
    (sdl2-ffi.functions:sdl-get-renderer-output-size sdl-renderer w h)
    (values (cffi:mem-ref w :int) (cffi:mem-ref h :int))))


(defun %draw-checkerboard (renderer
                           &aux
                             (sdl-renderer (media:renderer-native renderer)))
  (labels ((num-light (w h)
             (ceiling (* w h) 2))
           (num-dark (w h)
             (- (* w h) (num-light w h))))
    (multiple-value-bind (render-w render-h)
        (%renderer-size sdl-renderer)
      (let* ((board-size 16)
             (num-horz (ceiling render-w board-size))
             (num-vert (ceiling render-h board-size))
             (num-light (num-light num-horz num-vert))
             (num-dark (num-dark num-horz num-vert)))

        (autowrap:with-many-alloc ((r 'sdl2-ffi:sdl-rect)
                                   (light-rects 'sdl2-ffi:sdl-rect num-light)
                                   (dark-rects 'sdl2-ffi:sdl-rect num-dark))
          (setf (plus-c:c-ref r sdl2-ffi:sdl-rect :w) board-size)
          (setf (plus-c:c-ref r sdl2-ffi:sdl-rect :h) board-size)
          (loop
             :for i :below (* num-horz num-vert)
             :for col := (mod i num-horz)
             :for row := (truncate i num-horz)
             :for index := (truncate i 2)
             :do
             (setf (plus-c:c-ref r sdl2-ffi:sdl-rect :x) (* col board-size))
             (setf (plus-c:c-ref r sdl2-ffi:sdl-rect :y) (* row board-size))
             :when (= (mod col 2) (mod row 2)) :do
             (autowrap:memcpy (autowrap:c-aref light-rects index 'sdl2-ffi:sdl-rect) r :type 'sdl2-ffi:sdl-rect)
             :else :do
             (autowrap:memcpy (autowrap:c-aref dark-rects index 'sdl2-ffi:sdl-rect) r :type 'sdl2-ffi:sdl-rect))

          (sdl2:set-render-draw-color sdl-renderer 128 128 128 255)
          (sdl2:render-fill-rects sdl-renderer light-rects num-light)
          (sdl2:set-render-draw-color sdl-renderer 64 64 64 255)
          (sdl2:render-fill-rects sdl-renderer dark-rects num-dark))))))
