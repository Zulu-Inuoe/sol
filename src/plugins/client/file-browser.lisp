;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.plugin.client)

(defclass file-browser (ui:content-control)
  ((dir
    :type pathname
    :initform *default-pathname-defaults*
    :reader file-browser-dir)
   (pattern
    :type pathname
    :initform #P"*.*"
    :reader file-browser-pattern)
   (e_selected-item-changed
    :type event:event
    :initform (make-instance 'event:event :name "selected-item-changed")
    :reader e_selected-item-changed)
   (file-list
    :type ui:panel
    :reader %file-browser.file-list)
   (selected-item
    :type (or null file-browser-item)
    :initform nil
    :accessor %file-browser.selected-item))
  (:default-initargs
   :horizontal-content-alignment :stretch
    :vertical-content-alignment :stretch))

(defmethod initialize-instance :after ((comp file-browser)
                                       &key
                                         (dir *default-pathname-defaults*)
                                         (pattern #P"*.*")
                                         &allow-other-keys)
  (setf (slot-value comp 'dir)
        (uiop/pathname:ensure-absolute-pathname
         (uiop/pathname:ensure-directory-pathname dir)
         *default-pathname-defaults*))
  (setf (slot-value comp 'pattern)
        (make-pathname :name (pathname-name pattern)
                       :type (or (pathname-type pattern) :wild)))
  (let ((file-list
         (make-instance
          'ui:stack-panel
          :vertical-alignment :top
          :horizontal-alignment :left
          :orientation :vertical
          :spacing 2
          :margin (make-instance 'ui:margin :left 5 :top 5 :right 5 :bottom 5))))
    (setf (ui:content comp) file-list)
    (setf (slot-value comp 'file-list) file-list))
  (file-browser.refresh comp))

(defun file-browser.selected-item (comp)
  (if (%file-browser.selected-item comp)
      (merge-pathnames (filename (%file-browser.selected-item comp))
                       (file-browser-dir comp))
      nil))

(defun (setf file-browser-dir) (value comp
                                &aux
                                  (new-val
                                   (uiop/pathname:ensure-absolute-pathname
                                    (uiop/pathname:ensure-directory-pathname value)
                                    *default-pathname-defaults*))
                                  (old-val (file-browser-dir comp)))
  (unless (equal new-val old-val)
    (setf (slot-value comp 'dir) new-val)
    (file-browser.refresh comp))
  (file-browser-dir comp))

(defun (setf file-browser-pattern) (value comp
                                    &aux
                                      (new-val (make-pathname :name (pathname-name value)
                                                              :type (or (pathname-type value) :wild)))
                                      (old-val (file-browser-pattern comp)))
  (unless (equal new-val old-val)
    (setf (slot-value comp 'pattern) new-val)
    (file-browser.refresh comp))
  (file-browser-pattern comp))

(defun file-browser.refresh (comp
                             &aux
                               (file-list (%file-browser.file-list comp))
                               (pattern (file-browser-pattern comp))
                               (dir (file-browser-dir comp)))
  (dolist (c (copy-list (ui:children file-list)))
    (ui:remove-handler c 'ui:e_mouse-down comp)
    (ui:remove-child file-list c))

  (dolist (i (directory (merge-pathnames pattern dir)))
    (%file-browser.add-item comp (enough-namestring i dir)))

  (values))

(defun %file-browser.add-item (comp filename
                               &aux
                                 (file-list (%file-browser.file-list comp)))
  (let ((new-item
         (make-instance
          'file-browser-item :filename filename)))
    (ui:add-child
     file-list
     new-item)

    (ui:add-handler
     new-item
     'ui:e_mouse-down
     comp
     (lambda (comp args)
       (setf (ui:handled args) t)
       (%file-browser.on-item-mouse-down comp new-item)))))

(defun %file-browser.on-item-mouse-down (comp item
                                         &aux
                                           (new-val item)
                                           (old-val (%file-browser.selected-item comp)))
  (unless (eq new-val old-val)
    (when old-val
      (setf (%file-browser-item-is-selected old-val) nil))

    (setf (%file-browser.selected-item comp) item)

    (when new-val
      (setf (%file-browser-item-is-selected new-val) t))

    (event:event-notify
     (e_selected-item-changed comp)
     new-val)))
