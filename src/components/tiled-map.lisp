;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package :sol.components)

(define-component tiled-map ()
  ((file-path
    :type pathname
    :initform (error "tiled-map: must provide file-path")
    :initarg :file-path
    :reader file-path
    :property-p t)
   (map
    :type cl-tiled:tiled-map)
   (map-width
    :type integer
    :reader map-width)
   (map-height
    :type integer
    :reader map-height)
   (background-color
    :type (or null media:color)
    :reader background-color)
   (cached-layers
    :type list)))

(defmethod component-init ((comp tiled-map) entity)
  (setf (slot-value comp 'map)
        (cl-tiled:load-map (file-path comp)))
  (setf (slot-value comp 'map-width)
        (cl-tiled:map-width-pixels (slot-value comp 'map)))
  (setf (slot-value comp 'map-height)
        (cl-tiled:map-height-pixels (slot-value comp 'map)))
  (setf (slot-value comp 'background-color)
        (%tcolor->color (cl-tiled:map-background-color (slot-value comp 'map))))

  (subscribe-event
   (global-events entity)
   :on-render
   comp
   '%tiled-map-on-render)

  (%create-objects comp))

(defmethod component-uninit ((comp tiled-map) entity)
  (unsubscribe-event
   (global-events entity)
   :on-render
   comp)

  ;;Free up any cache we might have
  (dolist (cache (slot-value comp 'cached-layers))
    (media:render-destroy-target cache))

  (slot-makunbound comp 'background-color)
  (slot-makunbound comp 'map-height)
  (slot-makunbound comp 'map-width)
  (slot-makunbound comp 'map))

(defun %tcolor->color (tcolor)
  (declare (optimize (debug 3)))
  (if tcolor
      (media:color :r (cl-tiled:tiled-color-r tcolor)
                   :g (cl-tiled:tiled-color-g tcolor)
                   :b (cl-tiled:tiled-color-b tcolor)
                   :a (cl-tiled:tiled-color-a tcolor))
      media:*transparent*))

(defun %create-objects (comp
                          &aux
                            (entity-manager (entity-manager comp))
                            (x-off 0)
                            (y-off 0))
  (labels ((create-objects (layer)
             (dolist (obj (cl-tiled:object-group-objects layer))
               (let ((ent (create-entity entity-manager :name (cl-tiled:object-name obj))))
                 (typecase obj
                   (cl-tiled:rect-object
                    (add-component ent (make-instance 'rigid-body :kinematic-p t))
                    (add-component ent (make-instance
                                        'transform
                                        :location
                                        (vec3 :x (+ (cl-tiled:object-x obj) (/ (cl-tiled:rect-width obj) 2) x-off)
                                              :z (+ (cl-tiled:object-y obj) (/ (cl-tiled:rect-height obj) 2) y-off))))
                    (add-component ent (make-instance
                                        'rect-collider
                                        :width (cl-tiled:rect-width obj)
                                        :height (cl-tiled:rect-height obj))))
                   (cl-tiled:polygon-object
                    (add-component ent (make-instance 'rigid-body :kinematic-p t))
                    (add-component ent (make-instance
                                        'transform
                                        :location
                                        (vec3 :x (+ (cl-tiled:object-x obj) x-off)
                                              :z (+ (cl-tiled:object-y obj) y-off))))
                    (add-component ent (make-instance
                                        'poly-collider
                                        :vertices
                                        (mapcar (lambda (p) (vec3 :x (car p) :z (cdr p)))
                                                (cl-tiled:polygon-vertices obj)))))
                   (cl-tiled:text-object
                    (add-component ent (make-instance
                                        'transform
                                        :location
                                        (vec3 :x (+ (cl-tiled:object-x obj) x-off)
                                              :z (+ (cl-tiled:object-y obj) y-off))))
                    (add-component ent (make-instance
                                        'text
                                        :string (cl-tiled:text-string obj)
                                        :font (make-instance
                                               'media:font
                                               :family (cl-tiled:text-font-family obj)
                                               :size (cl-tiled:text-pixel-size obj)
                                               :bold (cl-tiled:text-bold obj)
                                               :italic (cl-tiled:text-italic obj)
                                               :underline (cl-tiled:text-underline obj)
                                               :strikethrough (cl-tiled:text-strikeout obj))
                                        :color (%tcolor->color (cl-tiled:text-color obj)))))
                   (t
                    (destroy-entity ent))))))
           (layers-create-objects (layers)
             (dolist (layer layers)
               (incf x-off (cl-tiled:layer-offset-x layer))
               (incf y-off (cl-tiled:layer-offset-y layer))
               (typecase layer
                 (cl-tiled:tile-layer
                  (dolist (c (cl-tiled:layer-cells layer))
                    (when (typep (cl-tiled:cell-tile c) 'cl-tiled:tiled-tileset-tile)
                      (incf x-off (cl-tiled:cell-x c))
                      (incf y-off (cl-tiled:cell-y c))
                      (when-let ((object-group (cl-tiled:tile-object-group (cl-tiled:cell-tile c))))
                        (create-objects object-group))
                      (decf y-off (cl-tiled:cell-y c))
                      (decf x-off (cl-tiled:cell-x c)))))
                 (cl-tiled:object-layer
                  (create-objects layer))
                 (cl-tiled:group-layer
                  (layers-create-objects (cl-tiled:group-layers layer))))
               (decf y-off (cl-tiled:layer-offset-y layer))
               (decf x-off (cl-tiled:layer-offset-x layer)))))
    (layers-create-objects (cl-tiled:map-layers (slot-value comp 'map)))))

(defun %cache-layers (comp renderer)
  (let ((loaded-tiles (make-hash-table))
        (opacity-mult 1.0))
    (labels ((ensure-tile (tile)
               (let ((ret (gethash tile loaded-tiles)))
                 (unless ret
                   (let ((image (cl-tiled:tile-image tile)))
                     (if image
                         (setf ret
                               (make-instance
                                'media:image
                                :path (cl-tiled:image-source image)
                                :width (cl-tiled:tile-width tile)
                                :height (cl-tiled:tile-height tile)
                                :x (cl-tiled:tile-pixel-x tile)
                                :y (cl-tiled:tile-pixel-y tile)
                                :color-key (%tcolor->color (cl-tiled:image-transparent-color image))))
                         (setf ret nil))
                     (setf (gethash tile loaded-tiles) ret)))
                 ret))
             (load-image (image)
               (break)
               (make-instance
                'media:image
                :path (cl-tiled:image-source image)
                :width (cl-tiled:image-width image)
                :height (cl-tiled:image-height image)))
             (cache-layers (layers)
               (loop :for layer :in layers
                  :if (cl-tiled:layer-visible layer)
                  :appending
                  (progn
                    (media:render-push-translate
                     renderer
                     (cl-tiled:layer-offset-x layer)
                     (cl-tiled:layer-offset-y layer))
                    (unwind-protect
                         (etypecase layer
                           (cl-tiled:tile-layer
                            (list
                             (media:render-with-target (renderer (cl-tiled:map-width-pixels
                                                                  (cl-tiled:layer-map layer))
                                                                 (cl-tiled:map-height-pixels
                                                                  (cl-tiled:layer-map layer))
                                                                 (* (cl-tiled:layer-opacity layer)
                                                                    opacity-mult))
                               (media:render-clear renderer)
                               (dolist (cell (cl-tiled:layer-cells layer))
                                 (let ((image (ensure-tile (cl-tiled:cell-tile cell))))
                                   (when image
                                     (media:render-draw-image
                                      renderer
                                      image
                                      (cl-tiled:cell-x cell)
                                      (cl-tiled:cell-y cell)))
                                   (unless image
                                     (print "failed to draw")))))))
                           (cl-tiled:image-layer
                            (if (cl-tiled:layer-image layer)
                                (list
                                 (media:render-with-target (renderer (cl-tiled:map-width-pixels
                                                                      (cl-tiled:layer-map layer))
                                                                     (cl-tiled:map-height-pixels
                                                                      (cl-tiled:layer-map layer))
                                                                     (* (cl-tiled:layer-opacity layer)
                                                                        opacity-mult))
                                   (media:render-clear renderer)
                                   (media:render-draw-image
                                    renderer
                                    (load-image (cl-tiled:layer-image layer))
                                    0 0)))
                                ()))
                           (cl-tiled:object-layer
                            nil)
                           (cl-tiled:group-layer
                            (let ((old-mult opacity-mult))
                              (setf opacity-mult (* opacity-mult (cl-tiled:layer-opacity layer)))
                              (prog1 (cache-layers (cl-tiled:group-layers layer))
                                (setf opacity-mult old-mult)))))
                      (media:render-pop renderer))))))
      (setf (slot-value comp 'cached-layers)
            (cache-layers (cl-tiled:map-layers (slot-value comp 'map)))))
    (dolist (image (hash-table-values loaded-tiles))
      (media:dispose image))
    (clrhash loaded-tiles)))

(defun %tiled-map-on-render (comp renderer)
  (unless (slot-boundp comp 'cached-layers)
    (%cache-layers comp renderer))

  (when-let* ((transform (get-component (component-entity comp) 'transform))
              (location (location transform)))
    (when-let ((background-color (background-color comp)))
      (media:render-draw-rect renderer (vec-x location) (vec-z location) (slot-value comp 'map-width) (slot-value comp 'map-height) :fill background-color))

    (dolist (cache (slot-value comp 'cached-layers))
      (media:render-draw-target renderer cache (vec-x location) (vec-z location)))))