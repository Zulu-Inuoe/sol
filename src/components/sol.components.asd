;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defsystem #:sol.components
  :name "sol.components"
  :version "0.0.0.0"
  :description "ECS Components for SOL"
  :author "Wilfredo Velázquez-Rodríguez <zulu.inuoe@gmail.com>"
  :license "zlib/libpng License <http://opensource.org/licenses/zlib-license.php>"
  :components
  ((:file "package")
   (:file "input-states" :depends-on ("package"))
   (:file "transform" :depends-on ("package"))
   (:file "camera" :depends-on ("package"))
   (:file "text" :depends-on ("package" "transform"))
   (:file "tiled-map" :depends-on ("package" "transform" "text"))
   (:file "physics" :depends-on ("package"))
   (:file "player-input" :depends-on ("package"))
   (:file "script" :depends-on ("package"))
   (:file "sprite" :depends-on ("package")))
  :depends-on
  (#:cl-tiled
   #:sol.core
   #:sol.ecs
   #:sol.event
   #:sol.input
   #:sol.math
   #:sol.media
   #:sol.serial
   #:sol.shapes))