;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package :sol.components)

(define-component sprite ()
  ((lanims-path
    :type pathname
    :reader lanims-path
    :property-p t)
   (animations
    :type list
    :initform nil
    :initarg :animations
    :reader animations)
   (current-animation
    :type (or null media:animation)
    :initform nil
    :reader current-animation)))

(defmethod initialize-instance :after ((sprite sprite)
                                       &key
                                         (path (error "sprite: must supply animation path"))
                                         &allow-other-keys)
  (etypecase path
    ((or string pathname)
     (setf path (pathname path))))
  (setf (slot-value sprite 'lanims-path) path))

(defmethod component-init ((sprite sprite) entity)
  (declare (ignore entity))
  (setf (slot-value sprite 'animations)
        (media:load-animations (lanims-path sprite)))
  (setf (slot-value sprite 'current-animation) (first (animations sprite))))

(defun (setf current-animation) (value sprite)
  (etypecase value
    (keyword (setf value (find value (animations sprite) :key #'media:name)))
    (media:animation))

  (unless (eq value (current-animation sprite))
    (when (current-animation sprite)
      (media:reset-animation (current-animation sprite)))
    (setf (slot-value sprite 'current-animation) value)))
