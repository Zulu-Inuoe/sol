;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defpackage #:sol.system.physics
  (:use #:alexandria #:cl #:sol.components #:sol.ecs #:sol.math)
  (:export
   #:physics-system))

(in-package #:sol.system.physics)

(defparameter +min-td+ -0.0001)
(defparameter +time-padding+ 0.0001)

(defstruct physics-entry
  (entity nil :type entity)
  (transform nil :type transform)
  (collider nil :type collider)
  (rigid-body nil :type (or null rigid-body)))

(defclass physics-system (system)
  ((entries
    :type (vector physics-entry)
    :initform (make-array 512
                          :element-type 'physics-entry
                          :adjustable t :fill-pointer 0
                          :initial-element (make-physics-entry
                                            :entity (make-instance 'entity)
                                            :transform (make-instance 'transform)
                                            :collider (make-instance 'collider)))
    :accessor physics-system-entries)
   (pending-col
    :type (vector list 16)
    :initform (make-array 16 :element-type 'list :initial-element nil :fill-pointer t)
    :reader pending-col)
   (collision-mtd-vecs
    :initform nil
    :accessor collision-mtd-vecs)
   (overlap-mtd-vecs
    :initform nil
    :accessor overlap-mtd-vecs)))

(defmethod system-configure ((system physics-system))
  (subscribe-event
   (event-manager system)
   :time-tick
   system
   '%physics-system-on-time-tick)

  (subscribe-event
   (event-manager system)
   :on-render
   system
   '%physics-system-on-render :priority 10000)

  (subscribe-event
   (event-manager system)
   :components-changed
   system
   '%physics-system-on-components-changed))

(defmethod system-unconfigure ((system physics-system))
  (unsubscribe-event
   (event-manager system)
   :components-changed
   system)

  (unsubscribe-event
   (event-manager system)
   :on-render
   system)

  (unsubscribe-event
   (event-manager system)
   :time-tick
   system))

(defun %physics-system-on-time-tick (system dt
                                     &aux (pending-col (pending-col system)))
  (loop
    :with col-vecs := ()
    :with ovr-vecs := ()
    :with entries := (physics-system-entries system)
    :with elen := (length entries)
    :for i :from 0 :below elen
    :for e1 := (aref entries i)
    :for t1 := (physics-entry-transform e1)
    :for b1 := (physics-entry-rigid-body e1)
    :for checked-entries := (list e1)
    :for effective-dt := dt
    :do
       (flet ((recheck-collisions ()
                (setf (fill-pointer pending-col) 0)
                (loop
                  :with max-dist := (+ 3600 (or (and b1 (* (vec-sqr-mag (velocity b1)) effective-dt)) 0))
                  :for j :from (1+ i) :below elen
                  :for e2 := (aref entries j)
                  :for t2 := (physics-entry-transform e2)
                  :for b2 := (physics-entry-rigid-body e2)
                  :unless
                  (or
                   ;;They are the same entry
                   (member e2 checked-entries)
                   ;;They are both kinematic
                   (and b1 (kinematic-p b1)
                        b2 (kinematic-p b2))
                   ;;They are too far appart (by a crappy metric)
                   (> (vec-sqr-dist (location t1) (location t2)) max-dist))
                  :do
                     (when-let ((col (%test-colliders effective-dt e1 e2 (vec- (location t1) (location t2)))))
                       (cond
                         ((and (minusp (first col))
                               (eq (third col) :collision))
                          ;;Overlap collision.
                          ;;Put it at the front
                          ;;and return immediately
                          (setf (fill-pointer pending-col) 1)
                          (setf (aref pending-col 0) col)
                          (return))
                         (t
                          (vector-push col pending-col))))
                  :finally
                     ;;Sort by collision time, with overlaps happening first
                     (setf pending-col (sort pending-col #'< :key #'first)))))
         ;;From the possible collisions, resolve by collision time
         ;;Overlapping collisions are time 0
         ;;event-type is either :trigger, :collision, or nil depending on
         ;;whether it's a trigger or collision response
         (recheck-collisions)
         (loop
           :with j := 0
           :while (< j (length pending-col))
           :for (td mtd-dir event-type e2) := (aref pending-col j)
           ;;A full blown collision
           :do
              (push e2 checked-entries)
              (case event-type
                (:collision
                 (cond
                   ((minusp td)          ;Overlap collision
                    ;;Adjust the location to fix the overlap
                    (setf (location t1) (vec- (location t1) (vec* mtd-dir (min td +min-td+))))
                    (push (cons (location t1) mtd-dir) ovr-vecs))
                   (t ;;Future collision Note: Safe to use b1 here since we need a velocity for future collisions
                    ;;Don't travel the full col-time
                    (let ((col-time (max (- td +time-padding+) 0)))
                      ;;Travel for as long as we can before colliding
                      (unless (zerop col-time)
                        (setf (location t1)
                              (vec+ (location t1)
                                    (vec* (velocity b1) col-time)))

                        ;;Decrease the effective dt, since we moved partially
                        (decf effective-dt col-time)))

                    ;;Oppose the velocity
                    (setf (velocity b1)
                          (vec- (velocity b1)
                                (vec-no-norm-project mtd-dir (velocity b1))))
                    (push (cons (location t1) mtd-dir) col-vecs)))

                 (send-event
                  (entity-events (physics-entry-entity e1))
                  :on-collision
                  (physics-entry-collider e2))
                 (send-event
                  (entity-events (physics-entry-entity e2))
                  :on-collision
                  (physics-entry-collider e1))

                 ;;Since we modified our location and velocity
                 ;;we need to re-calculate our collisions
                 (recheck-collisions)
                 (setf j 0))
                (:trigger
                 ;; Just a trigger, signal events and carry on
                 (when mtd-dir
                   (if (minusp td)
                       ;;Overlap trigger
                       (push (cons (location t1) (vec* mtd-dir (- td))) ovr-vecs)
                       ;;Future trigger
                       (push (cons (location t1) mtd-dir) col-vecs)))
                 (send-event
                  (entity-events (physics-entry-entity e1))
                  :on-trigger
                  (physics-entry-collider e2))
                 (send-event
                  (entity-events (physics-entry-entity e2))
                  :on-trigger
                  (physics-entry-collider e1))

                 (incf j))
                (t
                 (error "physics-system: invalid event type ~A" event-type)))

           :finally
              ;;Now that we've resolved all possible collisions,
              ;;move according to velocity
              (when (and b1 (not (kinematic-p b1)))
                (setf (location t1)
                      (vec+ (location t1)
                            (vec* (velocity b1) effective-dt))))))
       (setf (collision-mtd-vecs system) (nreverse col-vecs))
       (setf (overlap-mtd-vecs system) (nreverse ovr-vecs))))

(defun %physics-system-on-render (system renderer)

  (loop
     :with entries := (physics-system-entries system)
     :with elen := (length entries)
     :for i :from 0 :below elen
     :for e := (aref entries i)
     :for offset := (location (physics-entry-transform e))
     :for p := (poly (physics-entry-collider e))
     :do
     (loop
        :for index :below (shapes:poly-num-vertices p)
        :for vertex := (shapes:poly-vertex p index)
        :for edge := (shapes:poly-edge p index)
        :for p1 := (vec+ vertex offset)
        :for p2 := (vec+ p1 edge)
        :do
        (media:render-draw-line
         renderer
         (vec-x p1) (vec-z p1)
         (vec-x p2) (vec-z p2)
         :color media:*blue*))
     (loop
        :for index :below (shapes:poly-num-vertices p)
        :for vertex := (shapes:poly-vertex p index)
        :for px := (vec+ vertex offset)
        :do
        (media:render-draw-point renderer (vec-x px) (vec-z px) :color media:*red*)))

  (dolist (cons (overlap-mtd-vecs system))
    (let* ((offset (car cons))
           (dir-and-mag (cdr cons))
           (dir (vec-norm dir-and-mag))
           (mag (vec-mag dir-and-mag)))
      (%render-arrow
       renderer
       offset dir mag
       media:*red*)))

  (dolist (cons (collision-mtd-vecs system))
    (let* ((offset (car cons))
           (dir (cdr cons))
           (mag 25))
      (%render-arrow
       renderer
       offset dir mag
       media:*lime-green*))))

(defun %physics-system-on-components-changed (system entity
                                              &aux
                                                (transform (get-component entity 'transform))
                                                (collider (or (get-component entity 'poly-collider) (get-component entity 'rect-collider)))
                                                (rigid-body (get-component entity 'rigid-body))
                                                (entry (find entity (physics-system-entries system) :key #'physics-entry-entity)))
  (cond
    ((and entry (or (null transform) (null collider)))
     (setf (physics-system-entries system) (delete entry (physics-system-entries system))))
    (entry
     (setf (physics-entry-transform entry) transform
           (physics-entry-collider entry) collider
           (physics-entry-rigid-body entry) rigid-body))
    ((and transform collider)
     (vector-push-extend  (make-physics-entry
                           :entity entity
                           :transform transform
                           :collider collider
                           :rigid-body rigid-body)
                          (physics-system-entries system)))))

(defun %test-colliders (dt e1 e2 offset
                        &aux
                          (c1 (physics-entry-collider e1))
                          (c2 (physics-entry-collider e2))
                          (b1 (physics-entry-rigid-body e1)))
  (when-let ((event-type
              (cond
                ((or (trigger-p c1) (trigger-p c2))
                 :trigger)
                ((and b1 (not (kinematic-p b1)))
                 :collision)
                (t
                 nil))))
    (let ((velocity (if b1 (velocity b1) +vec-zero+)))
      (multiple-value-bind (col-mtd td)
          (physics:collide-vel-mtd
           (poly c1) (poly c2)
           offset velocity dt)
        (when col-mtd
          (list td col-mtd event-type e2))))))

(defun %render-arrow (renderer p d len color
                      &aux
                        (draw-len len)
                        (arrow-len (/ draw-len 3))
                        (angle (atan (- (vec-z d)) (vec-x d)))
                        (arrow-angle (/ pi 8))
                        (p2 (vec+ p (vec* d draw-len)))
                        (p3 (vec3 :x (- (vec-x p2) (* (cos (+ angle arrow-angle)) arrow-len))
                                  :z (+ (vec-z p2) (* (sin (+ angle arrow-angle)) arrow-len))))
                        (p4 (vec3 :x (- (vec-x p2) (* (cos (- angle arrow-angle)) arrow-len))
                                  :z (+ (vec-z p2) (* (sin (- angle arrow-angle)) arrow-len)))))
  (media:render-draw-line
   renderer
   (round (vec-x p)) (round (vec-z p))
   (round (vec-x p2)) (round (vec-z p2))
   :color color)
  (media:render-draw-line
   renderer
   (vec-x p2) (vec-z p2)
   (vec-x p3) (vec-z p3)
   :color color)
  (media:render-draw-line
   renderer
   (vec-x p2) (vec-z p2)
   (vec-x p4) (vec-z p4)
   :color color))
