;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defpackage #:sol.system.input
  (:use #:alexandria #:cl #:sol.components #:sol.ecs)
  (:export
   #:input-system))

(in-package #:sol.system.input)

(defclass input-system (ecs:system)
  ;; Input states that were updated 'this time tick'
  ;; Used to reset their "was-pressed" and "was-released"
  ((input-manager
    :type input:input-manager
    :initform (error "input-system: must supply input-manager")
    :initarg :input-manager
    :reader input-manager)
   (updated-inputs
    :type list
    :initform nil
    :accessor updated-inputs)
   ;; Input states that were updated 'this time tick' by non-button mouse inputs (x, y, wheel)
   ;; Because these mouse events provide no corresponding "stop" event, we simulate one
   (mouse-inputs
    :type list
    :initform nil
    :accessor mouse-inputs)
   ;; Last known mouse x position
   (mouse-x
    :type integer
    :initform 0
    :accessor mouse-x)
   ;; Last known mouse y position
   (mouse-y
    :type integer
    :initform 0
    :accessor mouse-y)))

(defmethod system-configure ((system input-system)
                             &aux (input (input-manager system)))

  (subscribe-event
   (event-manager system)
   :time-tick
   system
   '%on-time-tick
   :priority 1000)

  (event:event-subscribe
   (input:e_key input)
   system
   '%on-key)
  (event:event-subscribe
   (input:e_mouse-move input)
   system
   '%on-mouse)
  (event:event-subscribe
   (input:e_mouse-button input)
   system
   '%on-mouse-button)
  (event:event-subscribe
   (input:e_mouse-wheel input)
   system
   '%on-mouse-wheel)
  (event:event-subscribe
   (input:e_controller-button input)
   system
   '%on-controller-button)
  (event:event-subscribe
   (input:e_controller-axis input)
   system
   '%on-controller-axis))

(defmethod system-unconfigure ((system input-system)
                               &aux (input (input-manager system)))
  (unsubscribe-event
   (event-manager system)
   :time-tick
   system)

  (event:event-unsubscribe
   (input:e_key input)
   system)
  (event:event-unsubscribe
   (input:e_mouse-move input)
   system)
  (event:event-unsubscribe
   (input:e_mouse-button input)
   system)
  (event:event-unsubscribe
   (input:e_mouse-wheel input)
   system)
  (event:event-unsubscribe
   (input:e_controller-button input)
   system)
  (event:event-unsubscribe
   (input:e_controller-axis input)
   system))

(defparameter +%controller-axis-dead-zone+ 0.14)

(defun %on-time-tick (system args)
  (declare (ignore args))
  (dolist (input (updated-inputs system))
    (setf
     (input-was-pressed input) nil
     (input-was-released input) nil))
  (setf (updated-inputs system) ())

  (dolist (input (mouse-inputs system))
    (setf (input-is-held input) nil
          (input-value input) 0
          (input-was-pressed input) nil
          (input-was-released input) t
          (input-time-released input) (core:current-time-seconds core:*sol-context*))
    (push input (updated-inputs system)))
  (setf (mouse-inputs system) ()))

(defun %on-key (system args
                &aux
                  (dev (input:device args))
                  (scheme :keyboard/mouse)
                  (name (input:key args))
                  (heldp (input:key-pressed args))
                  (val (if heldp 1 0)))
  (%update-input system dev scheme name heldp val))

(defun %on-mouse (system args
                  &aux
                    (dev (input:device args))
                    (scheme :keyboard/mouse)
                    (name-x :mouse-x)
                    (heldp-x (/= (input:x args) (mouse-x system)))
                    (val-x (input:x args))
                    (name-y :mouse-y)
                    (heldp-y (/= (input:y args) (mouse-y system)))
                    (val-y (input:y args)))
  (%update-input system dev scheme name-x heldp-x val-x)
  (%update-input system dev scheme name-y heldp-y val-y)
  (setf (mouse-x system) val-x
        (mouse-y system) val-y))

(defun %on-mouse-button (system args
                         &aux
                           (dev (input:device args))
                           (scheme :keyboard/mouse)
                           (name (input:button args))
                           (heldp (input:button-pressed args))
                           (val (if heldp 1 0)))
  (%update-input system dev scheme name heldp val))

(defun %on-mouse-wheel (system args
                        &aux
                          (dev (input:device args))
                          (scheme :keyboard/mouse)
                          (name :mouse-wheel)
                          (heldp (/= (input:delta args) 0))
                          (val (input:delta args)))
  (%update-input system dev scheme name heldp val))

(defun %on-controller-axis (system args
                            &aux
                              (dev (input:device args))
                              (scheme :controller)
                              (name (input:axis args))
                              (val-deadzone-p
                               (<= (abs (input:value args))
                                   +%controller-axis-dead-zone+))
                              (heldp (not val-deadzone-p))
                              (val (if heldp (input:value args) 0)))
  (%update-input system dev scheme name heldp val))

(defun %on-controller-button (system args
                              &aux
                                (dev (input:device args))
                                (scheme :controller)
                                (name (input:button args))
                                (heldp (input:button-pressed args))
                                (val (if heldp 1 0)))
  (%update-input system dev scheme name heldp val))

(defun %update-input (system device scheme-type name heldp val)
  (do-components (entity-manager system)
      (entity-id (input player-input)
                 (states input-states))
    (when (%using-device input device)
      (dolist (input-state (%get-mapped-input-states
                            (input-map-chain input) states
                            scheme-type name))
        (%update-input-state input-state heldp val)
        (if (member name '(:mouse-wheel :mouse-x :mouse-y))
            (push input-state (mouse-inputs system))
            (push input-state (updated-inputs system)))))))

(defun %update-input-state (input heldp val)
  (let* ((was-pressed (and heldp (not (input-is-held input))))
         (time-pressed (or (and was-pressed (core:current-time-seconds core:*sol-context*))
                           (input-time-pressed input)))
         (was-released (and (not heldp) (input-is-held input)))
         (time-released (or (and was-released (core:current-time-seconds core:*sol-context*))
                            (input-time-released input))))
    (setf (input-is-held input) heldp
          (input-value input) val
          (input-was-pressed input) was-pressed
          (input-time-pressed input) time-pressed
          (input-was-released input) was-released
          (input-time-released input) time-released)))

(defun %using-device (player-input device-id)
  (or (null (input:devices (components:player-handle player-input)))
      (member device-id (input:devices (components:player-handle player-input)))))

(defun %get-mapped-input-states (input-map-chain input-states scheme-type input-name)
  (loop :for mapped-name :in
     (input:chain-get-mapped-input-names input-map-chain scheme-type input-name)
     :if (get-input input-states mapped-name)
     :collect :it))