;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defpackage #:sol.system.render
  (:nicknames #:system.render)
  (:use #:alexandria #:cl #:sol.components #:sol.ecs #:sol.math)
  (:export
   #:render-system))

(in-package #:sol.system.render)

(defclass render-camera ()
  ((x
    :initform 0 :initarg :x
    :accessor x)
   (y
    :initform 0 :initarg :y
    :accessor y)
   (width
    :initform 0 :initarg :width
    :accessor width)
   (height
    :initform 0 :initarg :height
    :accessor height)))

(defclass render-system (ecs:system)
  ((renderer
    :type media:renderer
    :initarg :renderer
    :initform (error "render-system: must supply renderer")
    :reader renderer)))

(defmethod initialize-instance :after ((system render-system)
                                       &key
                                         &allow-other-keys))

(defmethod system-configure ((system render-system))
  (ecs:subscribe-event
   (event-manager system)
   :time-tick
   system
   '%on-time-tick)

  (ecs:subscribe-event
   (event-manager system)
   :frame-tick
   system
   '%on-frame-tick))

(defmethod system-unconfigure ((system render-system))
  (ecs:unsubscribe-event
   (event-manager system)
   :time-tick
   system)

  (ecs:unsubscribe-event
   (event-manager system)
   :frame-tick
   system))

(defun %on-time-tick (system dt)
  ;;Update all the animations
  (do-components (entity-manager system)
      (entity-id
       (sprite sprite))
    (when (current-animation sprite)
      (media:update-animation
       (current-animation sprite)
       dt))))

(defun %on-frame-tick (system args
                       &aux
                         (renderer (renderer system))
                         (x-offset 0)
                         (y-offset 0))
  (declare (ignore args))

  (do-components (entity-manager system)
      (entity-id
       (transform transform)
       (camera camera))
    (when (activep camera)
      (setf x-offset (max 0 (- (vec-x (location transform)) (/ (media:renderer-width renderer) 2))))
      (setf y-offset (max 0 (- (vec-z (location transform)) (/ (media:renderer-height renderer) 2))))))

  (media:render-push-translate
   renderer
   (- x-offset)
   (- y-offset))
  (unwind-protect
       (progn
         (send-event
          (event-manager system)
          :on-render
          renderer)

         ;;Draw all the game entities
         (do-components (entity-manager system)
             (entity-id
              (transform transform)
              (sprite sprite))
           (when (current-animation sprite)
             (media:draw-animation
              (current-animation sprite)
              renderer
              :x (- (vec-x (location transform)) (/ (media:animation-width (current-animation sprite)) 2))
              :y (- (vec-z (location transform)) (/ (media:animation-height (current-animation sprite)) 2))))))
    (media:render-pop renderer)))