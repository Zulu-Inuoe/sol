;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defpackage #:sol.core
  (:use #:alexandria #:cl)
  (:nicknames #:core)
  (:export
   #:*sol-context*

   #:sol
   #:sol-init
   #:sol-uninit
   #:sol-run
   #:sol-exit
   #:running
   #:current-time-seconds

   #:e_time-tick
   #:e_time-ticks-completed

   #:get-plugin
   #:plugin-init
   #:plugin-uninit))

(in-package #:sol.core)

;;Multiplier applied in order to translate from some time amount into milliseconds
(defparameter +internal-time/ms+ (/ internal-time-units-per-second 1000))
(defparameter +internal-time/second+ internal-time-units-per-second)

(defparameter +ms/internal-time+ (/ 1000 internal-time-units-per-second))
(defparameter +seconds/internal-time+ (/ 1 internal-time-units-per-second))

(declaim (inline internal-time->seconds))
(defun internal-time->seconds (time)
  (* time +seconds/internal-time+))

(declaim (inline internal-time->ms))
(defun internal-time->ms (time)
  (* time +ms/internal-time+))

(declaim (inline seconds->internal-time))
(defun seconds->internal-time (seconds)
  (* seconds +internal-time/second+))

(declaim (inline ms->internal-time))
(defun ms->internal-time (ms)
  (* ms +internal-time/ms+))

(defparameter *sol-context* nil)

(defclass sol ()
  ((current-time-seconds
    :type real
    :initform 0
    :accessor current-time-seconds)
   (load-plugins
    :initform nil
    :initarg :load-plugins
    :reader load-plugins)
   (e_time-tick
    :documentation "Fired possibly multiple times per frame each clock tick with dt seconds"
    :type event:event
    :initform (make-instance 'event:event :name "time-tick")
    :reader e_time-tick)
   (e_time-ticks-completed
    :documentation "Fired once per frame after all time ticks complete"
    :type event:event
    :initform (make-instance 'event:event :name "time-ticks-completed")
    :reader e_time-ticks-completed)
   (plugins
    :initform nil
    :accessor plugins)
   (running
    :initform t
    :accessor running)))

(defun get-plugin (plugin-name)
  (if (symbolp plugin-name)
      (find plugin-name (plugins *sol-context*) :key (lambda (mod) (class-name (class-of mod))))
      (find plugin-name (plugins *sol-context*)
            :key (lambda (mod) (symbol-name (class-name (class-of mod))))
            :test #'string-equal)))

(defgeneric plugin-init (plugin)
  (:method (plugin)
    (declare (ignore plugin))))

(defgeneric plugin-uninit (plugin)
  (:method (plugin)
    (declare (ignore plugin))))

(defun sol-init (context
                 &aux
                   (*sol-context* context))
  (dolist (plugin (load-plugins context))
    (push
     (make-instance plugin)
     (plugins context)))

  (dolist (plugin (plugins context))
    (plugin-init plugin)))

(defun sol-uninit (context
                   &aux
                     (*sol-context* context))
  (dolist (plugin (reverse (plugins context)))
    (plugin-uninit plugin)))

(defun sol-run (context
                &key
                  (max-fps 100)
                &aux
                  (*sol-context* context))
  (setf (running context) t)
  (unwind-protect
       (let ((dt-seconds (float (/ 1 max-fps)))
             (current-time (get-internal-real-time))
             (accumulator-seconds 0.0)
             (too-fast nil)
             (frame-number 0))
         (loop
            :while (running context)
            :do
            (restart-case
                (progn
                  (let* ((new-time (get-internal-real-time))
                         (frame-time-seconds  (internal-time->seconds
                                               (- new-time current-time))))
                    (setf current-time new-time)

                    (incf accumulator-seconds frame-time-seconds)

                    (setf too-fast (< accumulator-seconds dt-seconds))
                    (unless too-fast
                      (loop
                        :do
                           (incf (current-time-seconds context) dt-seconds)
                           (with-simple-restart (skip-time-tick "Restart the current frame")
                             (event:event-notify (e_time-tick context) dt-seconds))
                           (decf accumulator-seconds dt-seconds)
                        :while (and (running context)
                                    (>= accumulator-seconds dt-seconds)))

                      (when (running context)
                        (with-simple-restart (skip-frame "Skip the current frame")
                          (event:event-notify (e_time-ticks-completed context) (incf frame-number))))))

                  ;;Throttle to max-fps
                  (when (and (running context)
                             too-fast)
                    (sleep (- dt-seconds accumulator-seconds)))

                  (incf frame-number))
              (exit ()
                (setf (running context) nil)))))
    (setf (running context) nil)))

(defun sol-exit (context
                 &aux
                   (*sol-context* context))
  (setf (running context) nil))
