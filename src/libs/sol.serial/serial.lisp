;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defpackage #:sol.serial
  (:use #:alexandria #:cl)
  (:nicknames #:serial)
  (:export
   #:serializable
   #:serialized-slots

   #:serialize
   #:serialize-file
   #:deserialize
   #:deserialize-file
   #:initialize-deserialized-instance))

(in-package #:sol.serial)

(defclass serializable ()
  ())

(defgeneric serialized-slots (object)
  (:method ((obj serializable))
    '()))

(defgeneric initialize-deserialized-instance (obj)
  (:method ((obj serializable))
    (declare (ignore obj))
    (values)))

(defun serialize (obj &key (stream t))
  (write (ms:marshal obj) :stream stream)
  (values))

(defun serialize-file (obj path)
  (with-open-file (stream path :direction :output :if-exists :supersede)
    (serialize obj :stream stream)))

(defun deserialize (stream)
  (ms:unmarshal (read stream)))

(defun deserialize-file (path)
  (with-open-file (stream path :direction :input :if-does-not-exist :error)
    (deserialize stream)))

(defmethod ms:class-persistant-slots ((obj serializable))
  (serialized-slots obj))

(defmethod ms:initialize-unmarshalled-instance ((obj serializable))
  (let* ((slots-needing-init (set-difference (mapcar #'closer-mop:slot-definition-name
                                                     (closer-mop:class-slots (class-of obj)))
                                             (serialized-slots obj))))
    (shared-initialize obj slots-needing-init))
  (initialize-deserialized-instance obj)

  obj)
