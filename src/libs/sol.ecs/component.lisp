;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.ecs)

(defclass component (serial:serializable)
  ((component-entity
    :type entity
    :reader component-entity))
  (:metaclass component-metaclass))

(defmethod entity-manager ((comp component))
  (entity-manager (component-entity comp)))

(defmethod global-events ((comp component))
  (global-events (component-entity comp)))

(defmethod entity-events ((comp component))
  (entity-events (component-entity comp)))

(defmethod serial:serialized-slots ((obj component)
                                      &aux (class (class-of obj)))
  (mapcar #'slot-definition-name
          (remove-if-not #'component-slot-property-p
                         (class-slots class))))

(defgeneric component-init (component entity)
  (:documentation
   "Called on a component when it is added to an entity")
  (:method (component entity)
    (declare (ignore component entity))))

(defgeneric component-uninit (component entity)
  (:documentation
   "Called on a component when it is removed from an entity.")
  (:method (component entity)
    (declare (ignore component entity))))

(defmethod component-init :around ((component component) entity)
  (setf (slot-value component 'component-entity) entity)
  (call-next-method))

(defmethod component-uninit :around ((component component) entity)
  (unwind-protect (call-next-method)
    (slot-makunbound component 'component-entity)))

(defmacro define-component (name direct-superclasses direct-slots &rest options)
  (with-gensyms (has-component-super)
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (let ((,has-component-super
              (find-if (lambda (c) (subtypep (find-class c) (find-class 'component)))
                       '(,@direct-superclasses))))
         (if ,has-component-super
             (defclass ,name (,@direct-superclasses)
               ,direct-slots
               (:metaclass component-metaclass)
               ,@options)
             (defclass ,name (component ,@direct-superclasses)
               ,direct-slots
               (:metaclass component-metaclass)
               ,@options))))))
