;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.ecs)

(defclass event-manager ()
  ((events
    :type hash-table :initform (make-hash-table)
    :reader events)))

(defun forward-event (event-manager name instance &key (priority 500))
  (let ((event (gethash name (events event-manager))))
    (unless event
      (setf event (make-instance 'event:event :name (symbol-name name)))
      (setf (gethash name (events event-manager)) event))
    (event:event-subscribe
     instance
     event
     (lambda (event args)
       (event:event-notify event args))
     :priority priority))
  (values))

(defun unforward-event (event-manager name instance)
  (when-let ((event (gethash name (events event-manager))))
    (event:event-unsubscribe
     instance
     event))
  (values))

(defun subscribe-event (event-manager name obj handler &key (priority 500))
  (let ((event (gethash name (events event-manager))))
    (unless event
      (setf event (make-instance 'event:event :name (symbol-name name)))
      (setf (gethash name (events event-manager)) event))
    (event:event-subscribe event obj handler :priority priority))
  (values))

(defun unsubscribe-event (event-manager name obj-or-handler)
  (when-let ((event (gethash name (events event-manager))))
    (event:event-unsubscribe event obj-or-handler)))

(defun send-event (event-manager name args)
  (when-let ((event (gethash name (events event-manager))))
    (event:event-notify event args)))