(defpackage #:sol.math
  (:nicknames #:math)
  (:use #:cl)
  (:export
   #:+vec-zero+
   #:+vec-right+
   #:+vec-up+
   #:+vec-forward+
   #:vec3
   #:vec-x
   #:vec-y
   #:vec-z
   #:vec+
   #:vec-
   #:vec*
   #:vec/
   #:vec-cross
   #:vec-dot
   #:vec-mag
   #:vec-sqr-mag
   #:vec-dist
   #:vec-sqr-dist
   #:vec-norm
   #:vec-project
   #:vec-no-norm-project

   #:+quat-identity+

   #:quaternion
   #:quat
   #:quat-w
   #:quat-x
   #:quat-y
   #:quat-z
   #:quat+
   #:quat-
   #:quat*
   #:quat/
   #:quat-conj
   #:quat-scale
   #:quat-norm
   #:quat-inverse
   #:quat-dot
   #:quat-lerp
   #:quat-slerp
   #:quat-look-rot
   #:quat-from-rot
   #:quat-from-euler
   #:quat-to-euler
   #:quat-rot-vec))

(in-package :sol.math)

(defclass vec3 (serial:serializable)
  ((x
    :type single-float :initform 0.0s0
    :initarg :x)
   (y
    :type single-float :initform 0.0s0
    :initarg :y)
   (z
    :type single-float :initform 0.0s0
    :initarg :z)))

(declaim (inline vec-x))
(declaim (ftype (function (vec3) (values single-float &optional)) vec-x))
(defun vec-x (v)
  (declare (type vec3 v))
  (the single-float (slot-value v 'x)))

(declaim (inline vec-y))
(declaim (ftype (function (vec3) (values single-float &optional)) vec-y))
(defun vec-y (v)
  (declare (type vec3 v))
  (the single-float (slot-value v 'y)))

(declaim (inline vec-z))
(declaim (ftype (function (vec3) (values single-float &optional)) vec-z))
(defun vec-z (v)
  (declare (type vec3 v))
  (the single-float (slot-value v 'z)))

(defmethod print-object ((v vec3) stream)
  (print-unreadable-object (v stream :type t)
    (format stream
            "(~5,2,,,'0F, ~5,2,,,'0F, ~5,2,,,'0F)"
            (vec-x v)
            (vec-y v)
            (vec-z v))))

(defmethod serial:serialized-slots ((obj vec3))
  '(x y z))

(declaim (inline vec3))
(declaim (ftype (function (&key (:x real) (:y real) (:z real)) (values vec3 &optional)) vec3))
(defun vec3 (&key (x 0.0s0) (y 0.0s0) (z 0.0s0))
  (declare (type real x y z))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (make-instance 'vec3 :x (float x single-float-epsilon) :y (float y single-float-epsilon) :z (float z single-float-epsilon)))

(defparameter +vec-zero+ (vec3))
(defparameter +vec-up+ (vec3 :y -1.0s0))
(defparameter +vec-forward+ (vec3 :z -1.0s0))
(defparameter +vec-right+ (vec3 :x 1.0s0))

(declaim (ftype (function (vec3 &rest (or real vec3)) (values vec3 &optional)) vec+))
(defun vec+ (v &rest quantities)
  (declare (type vec3 v))
  (declare (optimize speed))
  (let ((res-x (vec-x v))
        (res-y (vec-y v))
        (res-z (vec-z v)))
    (declare (type single-float res-x res-y res-z))

    (dolist (q quantities)
      (etypecase q
        (real
         (setf q (float q single-float-epsilon))
         (incf res-x q)
         (incf res-y q)
         (incf res-z q))
        (vec3
         (with-slots (x y z) q
           (declare (type single-float x y z))
           (incf res-x x)
           (incf res-y y)
           (incf res-z z)))))

    (vec3 :x res-x :y res-y :z res-z)))

(declaim (ftype (function (vec3 &rest (or real vec3)) (values vec3 &optional)) vec-))
(defun vec- (v &rest quantities)
  (declare (type vec3 v))
  (declare (optimize speed))
  ;;Special case if no quantities to subtract, negate
  (if (null quantities)
      (vec3
       :x (- (the single-float (vec-x v)))
       :y (- (the single-float (vec-y v)))
       :z (- (the single-float (vec-z v))))
      ;;Otherwise do subtraction as normal
      (let ((res-x (vec-x v))
            (res-y (vec-y v))
            (res-z (vec-z v)))
        (declare (type single-float res-x res-y res-z))

        (dolist (q quantities)
          (etypecase q
            (real
             (setf q (float q single-float-epsilon))
             (decf res-x q)
             (decf res-y q)
             (decf res-z q))
            (vec3
             (with-slots (x y z) q
               (declare (type single-float x y z))
               (decf res-x x)
               (decf res-y y)
               (decf res-z z)))))

        (vec3 :x res-x :y res-y :z res-z))))

(declaim (ftype (function (vec3 &rest real) (values vec3 &optional)) vec*))
(defun vec* (v &rest numbers)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (let ((res-x (vec-x v))
        (res-y (vec-y v))
        (res-z (vec-z v)))
    (declare (type single-float res-x res-y res-z))

    (macrolet ((incf* (place amt)
                 `(setf ,place (* ,place ,amt))))
      (dolist (q numbers)
        (etypecase q
          (real
           (setf q (float q single-float-epsilon))
           (incf* res-x q)
           (incf* res-y q)
           (incf* res-z q)))))

    (vec3 :x res-x :y res-y :z res-z)))

(declaim (ftype (function (vec3 real &rest real) (values vec3 &optional)) vec/))
(defun vec/ (v n &rest numbers)
  (declare (type vec3 v)
           (type real n))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (setf n (float n single-float-epsilon))
  (let ((res-x (/ (vec-x v) n))
        (res-y (/ (vec-y v) n))
        (res-z (/ (vec-z v) n)))
    (declare (type single-float res-x res-y res-z))

    (macrolet ((incf/ (place amt)
                 `(setf ,place (/ ,place ,amt))))
      (dolist (q numbers)
        (etypecase q
          (real
           (setf q (float q single-float-epsilon))
           (incf/ res-x q)
           (incf/ res-y q)
           (incf/ res-z q)))))

    (vec3 :x res-x :y res-y :z res-z)))

(declaim (ftype (function (vec3 vec3 &rest vec3) (values vec3 &optional)) vec-cross))
(defun vec-cross (v1 v2 &rest vectors)
  (declare (type vec3 v1 v2))
  (declare (optimize speed))
  (flet ((cross (x1 y1 z1 v)
           (declare (type single-float x1 y1 z1)
                    (type vec3 v))
           (let ((x2 (vec-x v))
                 (y2 (vec-y v))
                 (z2 (vec-z v)))
             (values
              (- (* y1 z2) (* z1 y2))
              (- (* z1 x2) (* x1 z2))
              (- (* x1 y2) (* y1 x2))))))
    (multiple-value-bind (res-x res-y res-z)
        (let ((x1 (vec-x v1))
              (y1 (vec-y v1))
              (z1 (vec-z v1)))
          (cross x1 y1 z1 v2))
      (dolist (v vectors)
        (declare (type vec3 v))
        (setf (values res-x res-y res-z) (cross res-x res-y res-z v)))
      (vec3 :x res-x :y res-y :z res-z))))

(declaim (ftype (function (vec3 vec3) (values single-float &optional)) vec-dot))
(defun vec-dot (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize speed))
  (let* ((x1 (vec-x v1))
         (y1 (vec-y v1))
         (z1 (vec-z v1))
         (x2 (vec-x v2))
         (y2 (vec-y v2))
         (z2 (vec-z v2)))
    (+ (* x1 x2)
       (* y1 y2)
       (* z1 z2))))

(declaim (ftype (function (vec3) (values (single-float 0.0s0) &optional)) vec-sqr-mag))
(defun vec-sqr-mag (v)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (let* ((x (vec-x v))
         (y (vec-y v))
         (z (vec-z v)))
    (+ (* x x)
       (* y y)
       (* z z))))

(declaim (ftype (function (vec3) (values single-float &optional)) vec-mag))
(defun vec-mag (v)
  (declare (type vec3 v))
  (declare (optimize speed))
  (the single-float (sqrt (vec-sqr-mag v))))

(declaim (ftype (function (vec3 vec3) (values (single-float 0.0s0) &optional)) vec-sqr-dist))
(defun vec-sqr-dist (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize speed))
  (let* ((x1 (vec-x v1))
         (y1 (vec-y v1))
         (z1 (vec-z v1))
         (x2 (vec-x v2))
         (y2 (vec-y v2))
         (z2 (vec-z v2))
         (dx (- x1 x2))
         (dy (- y1 y2))
         (dz (- z1 z2)))
    (+ (* dx dx)
       (* dy dy)
       (* dz dz))))

(declaim (ftype (function (vec3 vec3) (values single-float &optional)) vec-dist))
(defun vec-dist (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize speed))
  (the single-float (sqrt (vec-sqr-dist v1 v2))))

(declaim (ftype (function (vec3) (values vec3 &optional)) vec-norm))
(defun vec-norm (v)
  (declare (type vec3 v))
  (declare (optimize speed))
  (let ((length (vec-sqr-mag v)))
    (if (zerop length)
        (vec3)
        (vec/ v (the single-float (sqrt length))))))

(declaim (ftype (function (vec3 vec3) (values vec3 &optional)) vec-no-norm-project))
(defun vec-no-norm-project (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize speed))
  (let* ((x1 (vec-x v1))
         (y1 (vec-y v1))
         (z1 (vec-z v1))
         (x2 (vec-x v2))
         (y2 (vec-y v2))
         (z2 (vec-z v2))
         (len (+ (* x1 x2)
                 (* y1 y2)
                 (* z1 z2))))
    (vec3
     :x (* len x1)
     :y (* len y1)
     :z (* len z1))))

(declaim (ftype (function (vec3 vec3) (values vec3 &optional)) vec-project))
(defun vec-project (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize speed (safety 0) (debug 0)))
  (let* ((x1 (vec-x v1))
         (y1 (vec-y v1))
         (z1 (vec-z v1))
         (x2 (vec-x v2))
         (y2 (vec-y v2))
         (z2 (vec-z v2))
         (len (/ (+ (* x1 x2)
                    (* y1 y2)
                    (* z1 z2))
                 (sqrt
                  (+ (* x1 x1)
                     (* y1 y1)
                     (* z1 z1))))))
    ;; (declare (type single-float x1 y1 z1 x2 y2 z2 len))
    (vec3
     :x (* len x1)
     :y (* len y1)
     :z (* len z1))))

(defclass quaternion (serial:serializable)
  ((quat-w
    :type real :initform 1.0
    :initarg :w :accessor quat-w)
   (%v
    :type vec3
    :reader %v)))

(defmethod print-object ((q quaternion) stream)
  (print-unreadable-object (q stream :type t)
    (format stream "(~5,2,,,'0F, ~5,2,,,'0F, ~5,2,,,'0F, ~5,2,,,'0F)"
            (quat-w q)
            (quat-x q)
            (quat-y q)
            (quat-z q))))

(defmethod serial:serialized-slots ((obj quaternion))
  '(quat-w %v))

(defun quat (&key (w 0) (x 0) (y 0) (z 0))
  (declare (type real w x y z))
  (make-instance 'quaternion :w w :x x :y y :z z))

(defun quat-x (q)
  (vec-x (%v q)))

(defun (setf quat-x) (v q)
  (setf (vec-x (%v q)) v))

(defun quat-y (q)
  (vec-y (%v q)))

(defun (setf quat-y) (v q)
  (setf (vec-y (%v q)) v))

(defun quat-z (q)
  (vec-z (%v q)))

(defun (setf quat-z) (v q)
  (setf (vec-z (%v q)) v))

(defmethod initialize-instance :after ((q quaternion) &key (x 0) (y 0) (z 0))
  (setf (slot-value q '%v) (vec3 :x x :y y :z z)))

(defun quat-identity ()
  (make-instance 'quaternion))

(defvar +quat-identity+ (quat-identity))

(defun quat+ (q &rest quantities)
  (declare (type quaternion q))
  (let ((res-w (quat-w q))
        (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
    (declare (type real res-w)
             (type vec3 res-v))

    (dolist (q quantities)
      (etypecase q
        (real
         (incf res-w q)
         (setf res-v (vec+ res-v q)))
        (vec3
         (setf res-v (vec+ res-v q)))
        (quaternion
         (incf res-w (quat-w q))
         (setf res-v (vec+ res-v (%v q))))))

    (make-instance 'quaternion :w res-w :x (vec-x res-v) :y (vec-y res-v) :z (vec-z res-v))))

(defun quat- (q &rest quantities)
  (declare (type quaternion q))
  ;;Special case if no quantities to subtract
  ;;Negate quaternion (negate both angle and axis... same dir)
  (if (null quantities)
      (make-instance
       'quaternion
       :w (- (quat-w q))
       :x (- (quat-x q))
       :y (- (quat-y q))
       :z (- (quat-z q)))
      (let ((res-w (quat-w q))
            (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
        (declare (type real res-w)
                 (type vec3 res-v))

        (dolist (q quantities)
          (etypecase q
            (real
             (incf res-w q)
             (setf res-v (vec+ res-v q)))
            (vec3
             (setf res-v (vec+ res-v q)))
            (quaternion
             (incf res-w (quat-w q))
             (setf res-v (vec+ res-v (%v q))))))

        (make-instance 'quaternion :w res-w :x (quat-x res-v) :y (vec-y res-v) :z (vec-z res-v)))))

(defun quat* (q &rest quantities)
  (declare (type quaternion q))
  (let ((res-w (quat-w q))
        (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
    (dolist (q quantities)
      (etypecase q
        (real
         (setf res-w (* res-w q))
         (setf res-v (vec* res-v q)))
        (quaternion
         (let ((sa res-w)
               (sb (quat-w q))
               (a res-v)
               (b (%v q)))
           (declare (type real sa sb)
                    (type vec3 a b))
           (setf res-w (- (* sa sb)
                          (vec-dot a b)))
           (setf res-v (vec+ (vec* b sa)
                             (vec* a sb)
                             (vec-cross a b)))))))

    (make-instance 'quaternion :w res-w :x (quat-x res-v) :y (vec-y res-v) :z (vec-z res-v))))

(defun quat/ (q num-or-quat &rest quantities)
  (declare (type quaternion q)
           (type (or real quaternion) num-or-quat))

  (let ((res-w (quat-w q))
        (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
    (declare (type real res-w)
             (type vec3 res-v))

    (dolist (q (cons num-or-quat quantities))
      (etypecase q
        (real
         (setf res-w (/ res-w q))
         (setf res-v (vec/ res-v q)))
        (quaternion
         ;;Division of quaternion A by quaternion B is multiplying A by the multiplicative inverse of B
         (let ((res-quat
                (quat*
                 (make-instance 'quaternion :w res-w :x (vec-x res-v) :y (vec-y res-v) :z (vec-z res-v))
                 (quat-inverse q))))
           (setf res-w (quat-w res-quat))
           (setf res-v (%v res-quat))))))

    (make-instance 'quaternion :w res-w :x (vec-x res-v) :y (vec-y res-v) :z (vec-z res-v))))

(defun quat-conj (q)
  (declare (type quaternion q))
  (make-instance
   'quaternion
   :w (quat-w q)
   :x (- (quat-x q))
   :y (- (quat-y q))
   :z (- (quat-z q))))

(defun quat-scale (q)
  (declare (type quaternion q))
  (sqrt
   (+ (expt (quat-w q) 2)
      (expt (quat-x q) 2)
      (expt (quat-y q) 2)
      (expt (quat-z q) 2))))

(defun quat-norm (q)
  (declare (type quaternion q))
  (let ((length
         (quat-scale q)))
    (if (zerop length)
        (make-instance 'quaternion)
        (quat/ q length))))

(defun quat-inverse (q)
  (declare (type quaternion q))
  (quat/
   (quat-conj q)
   (+ (expt (quat-w q) 2)
      (expt (quat-x q) 2)
      (expt (quat-y q) 2)
      (expt (quat-z q) 2))))

(defun quat-dot (q1 q2)
  (declare (type quaternion q1 q2))
  (+ (* (quat-w q1) (quat-w q1))
     (vec-dot (%v q1) (%v q2))))

(defun quat-lerp (q1 q2 d)
  (declare (type quaternion q1 q2)
           (type real d))
   ;;Clamp it between 0 and 1
  (setf d (min (max d 0.0) 1.0))

  (quat-norm
   (quat+ (quat* q1 (- 1 d))
          (quat* q2 d))))

(defun quat-slerp (q1 q2 d)
  (declare (type quaternion q1 q2)
           (type real d))
 ;;Clamp it between 0 and 1
  (setf d (min (max d 0.0) 1.0))

  (let* ((dot (quat-dot q1 q2))
         (q3 q2))
    (when (< dot 0)
      (setf dot (- dot))
      (setf q3 (quat- q2)))

    (if (< dot 0.95)
        (let ((angle (acos dot)))
          (quat+ (quat* q1 (sin (* angle (- 1 d))))
                 (quat* q3 (/ (sin (* angle d))
                              (sin angle)))))
        ;;Small angles: Use lerp
        (quat-lerp q1 q3 d))))

(defun quat-from-rot (rot axis)
  (declare (type real rot)
           (type vec3 axis))
  (let ((rot-v (vec* axis (sin (/ rot 2)))))
    (make-instance
     'quaternion
     :w (cos (/ rot 2))
     :x (vec-x rot-v)
     :y (vec-y rot-v)
     :z (vec-z rot-v))))

(defun quat-look-rot (forward &optional (up +vec-up+))
  (declare (type vec3 forward up))
  (let* ((v1 (vec-norm forward))
         (v2 (vec-norm (vec-cross (vec-norm up) v1)))
         (v3 (vec-norm (vec-cross v1 v2)))
         (m00 (vec-x v2))
         (m01 (vec-y v2))
         (m02 (vec-z v2))
         (m10 (vec-x v3))
         (m11 (vec-y v3))
         (m12 (vec-z v3))
         (m20 (vec-x v1))
         (m21 (vec-y v1))
         (m22 (vec-z v2))
         (num8 (+ m00 m11 m22))
         (ret-quat (make-instance 'quaternion)))
    (cond
      ((> num8 0)
       (let ((num (sqrt (1+ num8))))
         (setf (quat-w ret-quat) (* num 0.5))
         (setf num (/ 0.5 num))
         (setf (quat-x ret-quat) (* (- m12 m21) num))
         (setf (quat-y ret-quat) (* (- m20 m02) num))
         (setf (quat-z ret-quat) (* (- m01 m10) num))))
      ((and (>= m00 m11) (>= m00 m22))
       (let* ((num7 (sqrt (- (1+ m00) m11 m22)))
              (num4 (/ 0.5 num7)))
         (setf (quat-w ret-quat) (* (- m12 m21) num4))
         (setf (quat-x ret-quat) (* 0.5 num7))
         (setf (quat-y ret-quat) (* (+ m01 m10) num4))
         (setf (quat-z ret-quat) (* (+ m02 m20) num4))))
      ((> m11 m22)
       (let* ((num6 (sqrt (- (1+ m11) m00 m22)))
              (num3 (/ 0.5 num6)))
         (setf (quat-w ret-quat) (* (- m20 m02) num3))
         (setf (quat-x ret-quat) (* (+ m10 m01) num3))
         (setf (quat-y ret-quat) (* 0.5 num6))
         (setf (quat-z ret-quat) (* (+ m21 m12) num3))))
      (t
       (let* ((num5 (sqrt (- (1+ m22) m00 m11)))
              (num2 (/ 0.5 num5)))
         (setf (quat-w ret-quat) (* (- m01 m10) num2))
         (setf (quat-x ret-quat) (* (+ m20 m02) num2))
         (setf (quat-y ret-quat) (* (+ m21 m12) num2))
         (setf (quat-z ret-quat) (* 0.5 num5)))))
    ret-quat))

(defun quat-from-euler (roll pitch yaw)
  (declare (type real roll pitch yaw))
  (let ((t0 (cos (* yaw 0.5)))
        (t1 (sin (* yaw 0.5)))
        (t2 (cos (* roll 0.5)))
        (t3 (sin (* roll 0.5)))
        (t4 (cos (* pitch 0.5)))
        (t5 (sin (* pitch 0.5))))
    (make-instance
     'quaternion
     :w (+ (* t0 t2 t4) (* t1 t3 t5))
     :x (- (* t0 t3 t4) (* t1 t2 t5))
     :y (+ (* t0 t2 t5) (* t1 t3 t4))
     :z (- (* t1 t2 t4) (* t0 t3 t5)))))

(defun quat-to-euler (q
                      &aux (ysqr (expt (quat-y q) 2)))
  (declare (type quaternion q))
  (let ((w (quat-w q))
        (x (quat-x q))
        (y (quat-y q))
        (z (quat-z q)))
    (values
     (let ((t0 (* 2 (+ (* w x) (* y z))))
           (t1 (- 1 (* 2 (+ (* x x) ysqr)))))
       (atan t0 t1))
     (let ((t2 (max
                -1
                (min 1
                     (* 2 (- (* w y) (* z x)))))))
       (asin t2))
     (let ((t3 (* 2 (+ (* w z) (* x y))))
           (t4 (- 1 (* 2 (+ ysqr (* z z))))))
       (atan t3 t4)))))

(defun quat-rot-vec (q v)
  (declare (type quaternion q)
           (vec3 v))
  (let* ((u (%v q))
         (s (quat-w q))
         (u-cross-v (vec-cross u v)))
    (vec+ (vec* u 2 (vec-dot u v))
          (vec* v (- (expt s 2) (vec-dot u u)))
          (vec* u-cross-v 2 s))))
