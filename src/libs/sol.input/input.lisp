;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.input)

(defclass window-event-args ()
  ((window-id
    :initarg :window-id
    :reader window-id)
   (event-type
    :initarg :event-type
    :reader event-type)
   (data1
    :initarg :data1
    :reader data1)
   (data2
    :initarg :data2
    :reader data2)))

(defclass text-input-event-args ()
  ((window-id
    :initarg :window-id
    :reader window-id)
   (text
    :type string
    :initform ""
    :initarg :text
    :reader text)))

(defconstant +kmod-none+    #x0000)
(defconstant +kmod-lshift+  #x0001)
(defconstant +kmod-rshift+  #x0002)
(defconstant +kmod-lctrl+   #x0040)
(defconstant +kmod-rctrl+   #x0080)
(defconstant +kmod-lalt+    #x0100)
(defconstant +kmod-ralt+    #x0200)
(defconstant +kmod-lmeta+   #x0400)
(defconstant +kmod-rmeta+   #x0800)
(defconstant +kmod-num+     #x1000)
(defconstant +kmod-caps+    #x2000)
(defconstant +kmod-mode+    #x4000)
(defconstant +kmod-ctrl+  (logior +kmod-lctrl+ +kmod-rctrl+))
(defconstant +kmod-shift+ (logior +kmod-lshift+ +kmod-rshift+))
(defconstant +kmod-alt+   (logior +kmod-lalt+ +kmod-ralt+))
(defconstant +kmod-meta+  (logior +kmod-lmeta+ +kmod-rmeta+))

(defclass input-event-args ()
  ((device
    :initarg :device
    :reader device)))

(defclass key/mouse-event-args (input-event-args)
  ((window-id
    :initarg :window-id
    :reader window-id)
   (modifiers
    :type integer
    :initform +kmod-none+
    :initarg :modifiers
    :reader modifiers)))

(defgeneric alt-down (args))
(defgeneric ctrl-down (args))
(defgeneric meta-down (args))
(defgeneric shift-down (args))

(defmethod alt-down ((args key/mouse-event-args))
  (/= (logand +kmod-alt+ (modifiers args)) 0))

(defmethod ctrl-down ((args key/mouse-event-args))
  (/= (logand +kmod-ctrl+ (modifiers args)) 0))

(defmethod meta-down ((args key/mouse-event-args))
  (/= (logand +kmod-meta+ (modifiers args)) 0))

(defmethod shift-down ((args key/mouse-event-args))
  (/= (logand +kmod-shift+ (modifiers args)) 0))

(defclass key-event-args (key/mouse-event-args)
  ((key
    :type keyword
    :initarg :key
    :reader key)
   (key-pressed
    :type boolean
    :initarg :pressed
    :reader key-pressed)))

(defclass mouse-event-args (key/mouse-event-args)
  ((x
    :type integer
    :initarg :x
    :reader x)
   (y
    :type integer
    :initarg :y
    :reader y)
   (%state
    :type integer
    :initarg :state
    :reader %state)))

(defun left-button-down (args)
  (/= (logand sdl2-ffi:+sdl-button-lmask+ (%state args))
      0))

(defun middle-button-down (args)
  (/= (logand sdl2-ffi:+sdl-button-mmask+ (%state args))
      0))

(defun right-button-down (args)
  (/= (logand sdl2-ffi:+sdl-button-rmask+ (%state args))
      0))

(defun x-button-1-down (args)
  (/= (logand sdl2-ffi:+sdl-button-x1mask+ (%state args))
      0))

(defun x-button-2-down (args)
  (/= (logand sdl2-ffi:+sdl-button-x2mask+ (%state args))
      0))

(defclass mouse-button-event-args (mouse-event-args)
  ((button
    :type keyword
    :initarg :button :reader button)
   (button-pressed
    :type boolean
    :initarg :button-pressed :reader button-pressed)
   (click-count
    :type integer
    :initarg :click-count :reader click-count)))

(defclass mouse-wheel-event-args (mouse-event-args)
  ((delta
    :type integer
    :initarg :delta :reader delta)))

(defclass controller-button-event-args (input-event-args)
  ((button
    :type keyword
    :initarg :button
    :reader button)
   (button-pressed
    :type boolean
    :initarg :button-pressed
    :reader button-pressed)))

(defclass controller-axis-event-args (input-event-args)
  ((axis
    :type keyword
    :initarg :axis
    :reader axis)
   (value
    :type number
    :initarg :value
    :reader value)))

(defclass input-gesture ()
  ((modifiers
    :type integer
    :initarg :modifiers
    :initform 0
    :reader modifiers)))

(defclass key-gesture ()
  ((key
    :type symbol
    :initarg :key
    :reader key)
   (modifiers
    :type integer
    :initarg :modifiers
    :initform 0
    :reader modifiers)))

(deftype mouse-action ()
  '(member
    :none
    :left-click
    :left-double-click
    :middle-click
    :middle-double-click
    :right-click
    :right-double-click
    :wheel-click))

(defgeneric matches (gesture input-event-args)
  (:method (gesture args)
    (declare (ignore gesture args))
    nil))

(defclass mouse-gesture (input-gesture)
  ((mouse-action
    :type mouse-action
    :initarg :mouse-action
    :reader mouse-action)))

(defmethod matches ((gesture key-gesture) (args key-event-args))
  (and (eq (key gesture) (key args))
       (= (modifiers gesture) (modifiers args))))

(defun %mouse-args->mouse-action (args)
  (case (button args)
    (:left
     (cond
       ((= (click-count args) 1) :left-click)
       ((= (click-count args) 2) :left-double-click)
       (t :none)))
    (:middle
     (cond
       ((= (click-count args) 1) :middle-click)
       ((= (click-count args) 2) :middle-double-click)
       (t :none)))
    (:right
     (cond
       ((= (click-count args) 1) :right-click)
       ((= (click-count args) 2) :right-double-click)
       (t :none)))
    (t
     :none)))

(defmethod matches ((gesture mouse-gesture) (args mouse-event-args))
  (and (not (eq (mouse-action gesture) :none))
       (eq (mouse-action gesture) (%mouse-args->mouse-action args))
       (= (modifiers gesture) (modifiers args))))