;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.net)

(defun make-guid ()
  "Create a randomly-generated 128-bit GUID"
  (random (ash 1 128)))

;;Multiplier applied in order to translate from some time amount into milliseconds
(defparameter +internal-time/ms+ (/ internal-time-units-per-second 1000))
(defparameter +internal-time/second+ internal-time-units-per-second)

(defparameter +ms/internal-time+ (/ 1000 internal-time-units-per-second))
(defparameter +seconds/internal-time+ (/ 1 internal-time-units-per-second))

(declaim (inline internal-time->seconds))
(defun internal-time->seconds (time)
  (* time +seconds/internal-time+))

(declaim (inline internal-time->ms))
(defun internal-time->ms (time)
  (* time +ms/internal-time+))

(declaim (inline seconds->internal-time))
(defun seconds->internal-time (seconds)
  (* seconds +internal-time/second+))

(declaim (inline ms->internal-time))
(defun ms->internal-time (ms)
  (* ms +internal-time/ms+))

(defparameter *max-packet-size* 512) ;512B per packet should be below IP MTU

(defparameter *magic-id* 15196)
(define-binary-struct packet-header ()
  (magic *magic-id* :binary-type u16)
  (protocol-id 0 :binary-type u16))

(defparameter *unreliable-protocol-id* 31470)
(define-binary-struct unreliable-packet-header ()
  (header
   (make-packet-header :protocol-id *unreliable-protocol-id*)
   :binary-type packet-header)
  (type 0 :binary-type u16))

(defparameter *hello-protocol-id* 47964)
(define-binary-struct hello-packet-header ()
  (header
   (make-packet-header :protocol-id *hello-protocol-id*)
   :binary-type packet-header))

(defparameter *reliable-protocol-id* 7926)
(define-binary-struct reliable-packet-header ()
  (header
   (make-packet-header :protocol-id *reliable-protocol-id*)
   :binary-type packet-header)
  (sequence 0 :binary-type u16)
  (ack 0 :binary-type u16)      ;This is the sequence # we ack
  (ack-mask 0 :binary-type u16) ;This is the other sequence #'s we ack
  (type 0 :binary-type u16))

(defparameter *reliable-protocol-ack-id* 8057)
(define-binary-struct reliable-packet-ack-header ()
  (header
   (make-packet-header :protocol-id *reliable-protocol-ack-id*)
   :binary-type packet-header)
  (sequence 0 :binary-type u16)
  (ack 0 :binary-type u16)
  (ack-mask 0 :binary-type u16))

(defparameter *acks-queue-max* 16)
(defparameter *sequence-max* (1- (expt 2 *acks-queue-max*)))

(defparameter *peer-timeout-ms* 5000)
(defparameter *peer-update-delay-ms* 33) ;Gets us about 30 messages per second
(defparameter *peer-packet-timeout-ms* 1000)

(defun %sequence-more-recent-p (s1 s2)
  (or (and (> s1 s2)
           (<= (- s1 s2)
               (/ *sequence-max* 2)))
      (and (> s2 s1)
           (> (- s2 s1)
              (/ *sequence-max* 2)))))

(declaim (inline %sequence-add))
(defun %sequence-add (s1 s2)
  (logand (+ s1 s2) *sequence-max*))

(declaim (inline %sequence-subtract))
(defun %sequence-subtract (s1 s2)
  (logand (- s1 s2) *sequence-max*))

(defclass peer ()
  ((remote-host
    :initarg :remote-host
    :reader remote-host)
   (remote-port
    :initarg :remote-port
    :reader remote-port)
   (remote-sequence-number
    :initarg :remote-sequence-number
    :accessor remote-sequence-number)
   (local-sequence-number
    :initform 0
    :accessor local-sequence-number)
   ;;The latest 17 sequential acks the peer has requested
   (requested-acks
    :initform (queues:make-queue :simple-queue)
    :reader requested-acks)
   (expecting-acks-lock
    :initform (bordeaux-threads:make-lock "peer acks lock")
    :reader expecting-acks-lock)
   ;;The latest 17 sequential acks we're expecting from the peer
   (expecting-acks
    :initform nil
    :accessor expecting-acks)
   (last-recv
    :initform (get-internal-real-time)
    :accessor last-recv)
   (last-send
    :initform (get-internal-real-time)
    :accessor last-send)))

(defmethod print-object ((p peer) stream)
  (print-unreadable-object (p stream :type t)
    (format stream "~S:~S" (remote-host p) (remote-port p))))

(defclass peer-event-args ()
  ((host
    :initarg :host
    :reader host)
   (port
    :initarg :port
    :reader port)))

(defclass message-received-event-args (peer-event-args)
  ((message-type
    :initarg :message-type
    :reader message-type)
   (payload
    :initarg :payload
    :reader payload)))

(defclass message-delivery-event-args (peer-event-args)
  ((message-id
    :initarg :message-id
    :reader message-id)))

(defclass comms ()
  ((interface
    :initform usocket:*wildcard-host*
    :initarg :interface
    :reader interface)
   (port
    :type integer
    :initform usocket:*auto-port*
    :initarg :port)
   (e_message-received
    :type event:event
    :initform (make-instance 'event:event :name "message-received")
    :reader e_message-received)
   (e_message-delivered
    :type event:event
    :initform (make-instance 'event:event :name "message-delivered")
    :reader e_message-delivered)
   (e_message-lost
    :type event:event
    :initform (make-instance 'event:event :name "message-lost")
    :reader e_message-lost)
   (e_peer-connected
    :type event:event
    :initform (make-instance 'event:event :name "peer-connected")
    :reader e_peer-connected)
   (e_peer-timeout
    :type event:event
    :initform (make-instance 'event:event :name "peer-timeout")
    :reader e_peer-timeout)
   (data-socket
    :type usocket:socket
    :accessor data-socket)
   (recv-thread
    :type bordeaux-threads:thread
    :accessor recv-thread)
   (send-thread
    :type bordeaux-threads:thread
    :accessor send-thread)
   (running
    :type boolean
    :initform nil
    :accessor running)
   (recv-buf
    :type (simple-array (unsigned-byte 8))
    :initform (make-array
               *max-packet-size*
               :element-type '(unsigned-byte 8)
               :fill-pointer 0)
    :reader recv-buf)
   (send-buf
    :type (simple-array (unsigned-byte 8))
    :initform (make-array
               *max-packet-size*
               :element-type '(unsigned-byte 8)
               :fill-pointer 0)
    :reader send-buf)
   (send-header
    :type packet-header
    :initform (make-packet-header)
    :reader send-header)
   (peers-lock
    :initform (bordeaux-threads:make-recursive-lock "sol peers lock")
    :reader peers-lock)
   (peers
    :initform nil
    :accessor peers)))

(defmethod port ((comms comms))
  (if (running comms)
      (usocket:get-local-port (data-socket comms))
      (slot-value comms 'port)))

(defun %read-packet-header (type packet)
  (assert (>= (length packet) (sizeof type)))
  (with-binary-input-from-vector (in packet)
    (read-binary type in)))

(defun %dispatch-packet (comms packet-type payload host port)
  (event:event-notify
   (e_message-received comms)
   (make-instance
    'message-received-event-args
    :host host
    :port port
    :message-type packet-type
    :payload payload)))

(defun %handle-unreliable-packet (comms header payload host port)
  (%dispatch-packet
   comms
   (unreliable-packet-header-type header)
   payload
   host port))

(defun %get-peer (comms host port)
  (bordeaux-threads:with-recursive-lock-held ((peers-lock comms))
    (find-if (lambda (p)
               (and
                (= (remote-port p) port)
                (equalp (remote-host p)
                        host)))
             (peers comms))))

(defun %create-peer (comms host port sequence-number)
  (let ((new-peer
         (make-instance
          'peer
          :remote-host host
          :remote-port port
          :remote-sequence-number sequence-number)))
    (bordeaux-threads:with-recursive-lock-held ((peers-lock comms))
      (push
       new-peer
       (peers comms)))
    new-peer))

(defun %ensure-peer (comms host port sequence-number)
  (bordeaux-threads:with-recursive-lock-held ((peers-lock comms))
    (or (%get-peer comms host port)
        (%create-peer comms host port sequence-number))))

(defun %remove-peer (comms peer)
  (bordeaux-threads:with-recursive-lock-held ((peers-lock comms))
    (setf (peers comms) (delete peer (peers comms)))))

(defun %handle-hello-packet (comms host port)
  (let ((peer (%get-peer comms host port)))
    (when (null peer)
      (setf peer (%create-peer
                  comms
                  host port
                  0))

      (event:event-notify
       (e_peer-connected comms)
       (make-instance
        'peer-event-args
        :host host :port port)))
    (setf (last-recv peer) (get-internal-real-time)))

  (values))

(defun %update-sequence-number (peer new-sequence-number)
  ;;Update the remote sequence numbers and acks
  ;;Queue up the old message number
  (queues:qpush (requested-acks peer)
                (remote-sequence-number peer))

  ;;Set the new one
  (setf (remote-sequence-number peer) new-sequence-number)

  ;;Remove stale requested acks
  (loop :while (and (queues:qtop (requested-acks peer))
                    (> (%sequence-subtract
                        (queues:qtop (requested-acks peer))
                        new-sequence-number)
                       *acks-queue-max*))
     :do (queues:qpop (requested-acks peer))))

(defun %decode-ack-mask (ack-base ack-mask)
  (cons
   ack-base
   (loop
      :for b :from 0 :upto 31
      :if (logbitp b ack-mask)
      :collect (%sequence-subtract ack-base (1+ b)))))

(defun %receive-acks (comms peer ack ack-mask)
  (let ((received-acks nil))
    (bordeaux-threads:with-lock-held ((expecting-acks-lock peer))
      (dolist (ack (%decode-ack-mask ack ack-mask))
        (when (find ack (expecting-acks peer) :key #'car)
          (setf (expecting-acks peer)
                (delete ack (expecting-acks peer) :key #'car))
          (push ack received-acks))))

    ;;Notify about the received acks
    (dolist (ack received-acks)
      ;; (format t "Received ACK for message '~A'~%" ack)
      (event:event-notify
       (e_message-delivered comms)
       (make-instance
        'message-delivery-event-args
        :message-id ack
        :host (remote-host peer)
        :port (remote-port peer))))))

(defun %handle-reliable-packet (comms header payload host port)
  ;; (format t "~A: Handling reliable proto~%" (port comms))
  (let ((peer (%ensure-peer
               comms
               host port
               (reliable-packet-header-sequence header))))
    ;; (format t "~A: peers: ~A~%" (port comms) (peers comms))
    (when (%sequence-more-recent-p
           (reliable-packet-header-sequence header)
           (remote-sequence-number peer))
      (%update-sequence-number
       peer
       (reliable-packet-header-sequence header)))

    (setf (last-recv peer) (get-internal-real-time))

    ;;Receive its acks
    (%receive-acks
     comms
     peer
     (reliable-packet-header-ack header)
     (reliable-packet-header-ack-mask header))

    ;;And deliver its payload
    (%dispatch-packet
     comms
     (reliable-packet-header-type header)
     payload
     host port)))

(defun %get-packet-payload (type packet)
  (make-array
   (- (length packet) (sizeof type))
   :element-type '(unsigned-byte 8)
   :displaced-to packet
   :displaced-index-offset
   (sizeof type)))

(defun %handle-reliable-ack-packet (comms header host port)
  ;; (format t "~A: Handling reliable proto ack from ~A:~A~%" (port comms) host port)
  (let ((peer (%ensure-peer
               comms
               host port
               (reliable-packet-ack-header-sequence header))))
    ;; (format t "~A: peers: ~A~%" (port comms) (peers comms))
    (when (%sequence-more-recent-p
           (reliable-packet-ack-header-sequence header)
           (remote-sequence-number peer))
      (%update-sequence-number
       peer
       (reliable-packet-ack-header-sequence header)))

    (setf (last-recv peer) (get-internal-real-time))

    ;;Receive its acks
    (%receive-acks
     comms
     peer
     (reliable-packet-ack-header-ack header)
     (reliable-packet-ack-header-ack-mask header))))

(defun %handle-packet (comms packet host port)
  ;;Read the header
  (when-let* ((header (and (>= (length packet) (sizeof 'packet-header))
                           (%read-packet-header 'packet-header packet)))
              (magic-matches-p (= (packet-header-magic header)
                                  *magic-id*)))
    ;; (format t "~A: Magic match. Protocol: ~A~%" (port comms) (packet-header-protocol-id header))
    (switch ((packet-header-protocol-id header))
      (*unreliable-protocol-id*
       (%handle-unreliable-packet
        comms
        (%read-packet-header 'unreliable-packet-header packet)
        (%get-packet-payload 'unreliable-packet-header packet)
        host port))
      (*hello-protocol-id*
       (%handle-hello-packet
        comms
        host port))
      (*reliable-protocol-id*
       (%handle-reliable-packet
        comms
        (%read-packet-header 'reliable-packet-header packet)
        (%get-packet-payload 'reliable-packet-header packet)
        host port))
      (*reliable-protocol-ack-id*
       (%handle-reliable-ack-packet
        comms
        (%read-packet-header
         'reliable-packet-ack-header
         packet)
        host port))
      (t
       (error "Unknown protocol id in packet: ~A~%"
              (packet-header-protocol-id header))))))

(defun %recv-worker-fn (comms
                              &aux
                                (recv-buf (recv-buf comms))
                                (*endian* :big-endian))
  (loop
     :while (running comms)
     :doing
     (handler-case
         (progn
           (setf (fill-pointer recv-buf) (1- *max-packet-size*))
           (multiple-value-bind (buf len host port)
               (usocket:socket-receive
                (data-socket comms) recv-buf nil)
             (declare (ignore buf))
             ;; (format t "~A: Received packet len ~A~%" (port comms) len)
             (setf (fill-pointer recv-buf) len)
             (%handle-packet comms recv-buf host port)))
       #+sbcl
       (sb-bsd-sockets:socket-error (err)
         (switch ((sb-bsd-sockets::socket-error-errno err))
           #+win32
           (0)     ;Catch a spurious false error
           (10054) ;Catch the WSAECONRESET error
           (t (format t "SBCL Socket error leaked: '~A'" err)))
         )
       (usocket:socket-error (cnd)
         (format t "~S~%" (type-of cnd))
         (format t "~A: Comms recv worker caught condition:~%~S~%" (port comms) cnd))))
  (values))

(defun %update-peer (comms peer)
  ;;If there are any messages to send, send them
  ;;See if this client is gone now..
  (cond
    ((> (internal-time->ms
         (- (get-internal-real-time)
            (last-recv peer)))
        *peer-timeout-ms*)
     (%remove-peer comms peer)

     ;;Notify that the peer is gone
     (event:event-notify
      (e_peer-timeout comms)
      (make-instance
       'peer-event-args
       :host (remote-host peer)
       :port (remote-port peer))))
    ((> (internal-time->ms
         (- (get-internal-real-time)
            (last-send peer)))
        *peer-update-delay-ms*)
     ;;Send an ack
     (let ((header (make-reliable-packet-ack-header
                    :sequence (local-sequence-number peer)
                    :ack (remote-sequence-number peer)
                    :ack-mask (%encode-ack-mask
                               (remote-sequence-number peer)
                               (requested-acks peer))))
           (*endian* :big-endian))
       ;; (format t "Sending periodic ack~%")
       (setf (fill-pointer (send-buf comms)) 0)
       (with-binary-output-to-vector (out-strm (send-buf comms))
         (write-binary 'reliable-packet-ack-header out-strm header))
       ;; (format t "~A: Sending packet of size~A~%" (port comms) (length (send-buf comms)))
       (usocket:socket-send
        (data-socket comms) (send-buf comms) (length (send-buf comms))
        :host (remote-host peer)
        :port (remote-port peer)))
     (setf (last-send peer) (get-internal-real-time))))

  ;;Check for any messages that are probably dead now
  (let ((removals nil))
    (bordeaux-threads:with-lock-held ((expecting-acks-lock peer))
      (setf removals
            (loop
               :for cons :in (expecting-acks peer)
               :if  (> (internal-time->ms
                        (- (get-internal-real-time)
                           (cdr cons)))
                       *peer-packet-timeout-ms*)
               :collect cons))
      (setf (expecting-acks peer) (nset-difference (expecting-acks peer)
                                                      removals)))

    (dolist (r removals)
      ;; (format t "~A: Packet was lost: ~A~%" (port comms) r)
      (event:event-notify
       (e_message-lost comms)
       (make-instance
        'message-delivery-event-args
        :message-id (car r)
        :host (remote-host peer)
        :port (remote-port peer))))))

(defun %send-worker-fn (comms
                        &aux
                          (messages-per-second 30)
                          (ms-per-message (/ 1000 messages-per-second))
                          (*endian* :big-endian))
  (loop
     :for current-time := (get-internal-real-time) :then new-time
     :for new-time := (get-internal-real-time)
     :for elapsed-time := (internal-time->ms (- new-time current-time))
     :for accum := elapsed-time :then (+ accum elapsed-time)
     :while (running comms)
     :if (>= accum ms-per-message)
     :do
     (handler-case
         (progn
           (decf accum ms-per-message)
           (bordeaux-threads:with-recursive-lock-held ((peers-lock comms))
             (dolist (peer (peers comms))
               (%update-peer comms peer))))
       (condition (cnd)
         (format t "~A:Comms send worker caught condition:~%~A~%" (port comms) cnd)))
     :else
     :do (sleep (/ (- ms-per-message accum)
                   1000))))

(defun comms-start (comms)
  (assert (not (running comms)))

  (setf (data-socket comms)
        (usocket:socket-connect
         nil nil
         :protocol :datagram
         :element-type '(unsigned-byte 8)
         :local-host (interface comms)
         :local-port (port comms)))

  (handler-bind ((error
                  (lambda (c)
                    (declare (ignore c))
                    (setf (running comms) nil)
                    (when (recv-thread comms)
                      (%wake-recv-worker comms)
                      (setf (recv-thread comms) nil))
                    (when (send-thread comms)
                      (setf (send-thread comms) nil))
                    (usocket:socket-close (data-socket comms))
                    (setf (data-socket comms) nil))))
    (setf (running comms) t)
    (format t "Comms with bindings:~%  ~A:~A~%"
            (usocket:get-local-port (data-socket comms))
            (usocket:get-local-address (data-socket comms)))
    (setf (recv-thread comms)
          (bordeaux-threads:make-thread
           (lambda () (%recv-worker-fn comms))
           :name "sol comms recv thread"))
    (setf (send-thread comms)
          (bordeaux-threads:make-thread
           (lambda () (%send-worker-fn comms))
           :name "sol comms send thread")))

  (values))

(defun %wake-recv-worker (comms)
  (usocket:socket-send
   (data-socket comms) (make-array 0 :element-type '(unsigned-byte 8))
   nil
   :port (usocket:get-local-port (data-socket comms))
   :host #(127 0 0 1)))

(defun comms-stop (comms)
  (assert (running comms))

  (setf (running comms) nil)
  ;;We need to wake up the recv worker..
  (%wake-recv-worker comms)

  (bordeaux-threads:join-thread (recv-thread comms))
  (bordeaux-threads:join-thread (send-thread comms))

  (setf (recv-thread comms) nil)
  (setf (send-thread comms) nil)

  (usocket:socket-close (data-socket comms))
  (setf (data-socket comms) nil)
  (values))

(defun comms-send-unreliable (comms host port packet-type packet-data)
  (setf (fill-pointer (send-buf comms)) 0)

  (let ((*endian* :big-endian)
        (header (make-unreliable-packet-header :type packet-type)))
    (with-binary-output-to-vector (out-strm (send-buf comms))
      (write-binary 'unreliable-packet-header out-strm header)
      (loop :for b :across packet-data
         :do (write-binary 'u8 out-strm b))))
  ;; (format t "~A: Sending packet of size~A~%" (port comms) (length (send-buf comms)))
  (usocket:socket-send
   (data-socket comms) (send-buf comms) (length (send-buf comms))
   :host host :port port)
  (values))

(defun %encode-ack-mask (ack-base acks-queue)
  (let ((ret-mask 0))
    (flet ((acc (ack-number)
             (let ((ack-diff (%sequence-subtract ack-base ack-number)))
               ;;1- because we don't need to include the base itself
               ;;since that one has a field allocated to it
               (assert (> ack-diff 0))
               (setf ret-mask (logior ret-mask (ash 1 (1- ack-diff)))))))
      (queues:map-queue #'acc acks-queue))
    ret-mask))

(defun comms-send-hello (comms host port)
  (setf (fill-pointer (send-buf comms)) 0)
  (let ((*endian* :big-endian)
        (header
         (make-hello-packet-header)))
    (with-binary-output-to-vector (out-strm (send-buf comms))
      (write-binary 'hello-packet-header out-strm header))

    (usocket:socket-send
     (data-socket comms) (send-buf comms) (length (send-buf comms))
     :host host :port port))
  0)

(defun comms-send-reliable (comms host port packet-type packet-data)
  (when-let ((peer (%get-peer comms host port)))
    (setf (fill-pointer (send-buf comms)) 0)

    (incf (local-sequence-number peer))
    ;;Add to the list of expectent acks the message number as well as the time

    (push (cons (local-sequence-number peer) (get-internal-real-time))
          (expecting-acks peer))

    ;;Compose a message with the latest acks the client wants
    (let ((*endian* :big-endian)
          (header (make-reliable-packet-header
                   :type packet-type
                   :sequence (local-sequence-number peer)
                   :ack (remote-sequence-number peer)
                   :ack-mask (%encode-ack-mask
                              (remote-sequence-number peer)
                              (requested-acks peer)))))
      (with-binary-output-to-vector (out-strm (send-buf comms))
        (write-binary 'reliable-packet-header out-strm header)
        (loop :for b :across packet-data
           :do (write-binary 'u8 out-strm b)))
      ;; (format t "~A: Sending packet of size~A~%" (port comms) (length (send-buf comms)))
      (usocket:socket-send
       (data-socket comms) (send-buf comms) (length (send-buf comms))
       :host host :port port))
    (setf (last-send peer) (get-internal-real-time))

    ;;Return the ID of the packet
    (local-sequence-number peer)))