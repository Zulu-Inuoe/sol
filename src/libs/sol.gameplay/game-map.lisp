;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.game)

(defclass layer ()
  ((name
    :type string
    :initarg :name
    :reader name)
   (opacity
    :type float
    :initarg :opacity
    :reader opacity)))

(defclass tile ()
  ((image
    :type media:image
    :initarg :image
    :reader image)
   (col
    :type integer
    :initarg :col
    :reader col)
   (row
    :type integer
    :initarg :row
    :reader row)))

(defclass tile-layer (layer)
  ((tiles
    :type list
    :initarg :tiles
    :reader tiles)))

(defclass object ()
  ((id
    :type integer
    :initarg :id
    :reader id)
   (name
    :type string
    :initarg :name
    :reader name)
   (x
    :type integer
    :initarg :x
    :reader x)
   (y
    :type integer
    :initarg :y
    :reader y)
   (rotation
    :type float
    :initarg :rotation
    :reader rotation)))

(defclass gid-object (object)
  ((gid
    :type integer
    :initarg :gid
    :reader gid)))

(defclass non-gid-object (object)
  ((width
    :type integer
    :initarg :width
    :reader width)
   (height
    :type integer
    :initarg :height
    :reader height)))

(defclass object-layer (layer)
  ((objects
    :type list
    :initarg :objects
    :reader objects)))

(defclass image-layer (layer)
  ((image
    :type media:image
    :initarg :image
    :reader image)))

(defclass game-map ()
  ((width
    :initarg :width
    :reader width)
   (height
    :initarg :height
    :reader height)
   (tile-width
    :initarg :tile-width
    :reader tile-width)
   (tile-height
    :initarg :tile-height
    :reader tile-height)
   (layers
    :type list
    :initarg :layers
    :reader layers)
   (background-color
    :type media:color
    :initarg :background-color
    :reader background-color)))

(defun draw-map (game-map)
  (dolist (layer (layers game-map))
    (draw-layer layer)))

(defgeneric draw-layer (layer))

(defmethod draw-layer ((layer tile-layer))
  (dolist (tile (tiles layer))
    (draw-tile tile)))

(defmethod draw-layer ((layer image-layer))
  (media:draw-image
   (image layer)))

(defun draw-tile (tile)
  (media:draw-image
   (image tile)
   :x (* (col tile) (media:image-width (image tile)))
   :y (* (row tile) (media:image-height (image tile)))))