;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.game)

(defclass game-world ()
  ((game-maps
    :initform nil
    :accessor game-maps)))

(defun game-world-start (game-world)
  ;;TODO TEMP Dev code
  (push
   (make-instance
    'game-map
    :map-path "resources/maps/stage1.lmap")
   (game-maps game-world))

  (dolist (map (game-maps game-world))
    (map-init map)))

(defun game-world-stop (game-world)
  (dolist (map (game-maps game-world))
    (map-uninit map)))

(defun game-world-time-tick (game-world dt)
  (dolist (map (game-maps game-world))
    (map-time-tick map dt)))