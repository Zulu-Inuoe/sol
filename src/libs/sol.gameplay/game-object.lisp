;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.game)

(defclass game-object ()
  ((id
    :initform nil
    :initarg :id
    :accessor id)
   (movement-speed
    :initform 0
    :initarg :movement-speed
    :accessor movement-speed)
   (x
    :initform 0
    :initarg :x
    :accessor x)
   (y
    :initform 0
    :initarg :y
    :accessor y)
   (z
    :initform 0
    :initarg :z
    :accessor z)
   (x-vel
    :initform 0
    :accessor x-vel)
   (y-vel
    :initform 0
    :accessor y-vel)
   (z-vel
    :initform 0
    :accessor z-vel)
   (right
    :initform nil
    :accessor right)
   (up
    :initform nil
    :accessor up)
   (left
    :initform nil
    :accessor left)
   (down
    :initform nil
    :accessor down)
   (jump
    :initform nil
    :accessor jump)))

(defgeneric game-object-init (obj)
  (:method (obj)
    (declare (ignore obj))))

(defgeneric game-object-time-tick (obj dt)
  (:method (obj dt)
    (declare (ignore obj dt))))

(defgeneric game-object-uninit (obj)
  (:method (obj)
    (declare (ignore obj))))

(defmethod game-object-time-tick ((obj game-object) dt)
  (with-slots (movement-speed x y z x-vel y-vel z-vel up down left right) obj
    (cond
      (right (setf x-vel  movement-speed))
      (left (setf x-vel (- movement-speed)))
      (t (setf x-vel 0)))
    (cond
      (up (setf y-vel (- movement-speed)))
      (down (setf y-vel movement-speed))
      (t (setf y-vel 0)))

    (incf x (* x-vel dt))
    (incf y (* y-vel dt))
    (incf z (* z-vel dt))))