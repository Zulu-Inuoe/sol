;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.ui)

(defclass window (content-control)
  ((window
    :type sdl2-ffi:sdl-window
    :reader window)
   (renderer
    :type media:renderer
    :reader renderer)
   (focus-manager
    :initform (make-instance 'focus-manager)
    :reader focus-manager)
   (e_window-closed
    :type event:event
    :initform (make-instance 'event:event :name "window-closed")
    :reader e_window-closed))
  (:default-initargs
   :horizontal-content-alignment :stretch
    :vertical-content-alignment :stretch))

(defun window-native (comp)
  (window comp))

(defun window-renderer (comp)
  (renderer comp))

(defun window-left (comp)
  (cffi:with-foreign-objects ((x :int))
    (sdl2-ffi.functions:sdl-get-window-position
     (window comp) x (cffi:null-pointer))
    (cffi:mem-ref x :int)))

(defun (setf window-left) (value comp)
  (setf value (round value))
  (sdl2-ffi.functions:sdl-set-window-position
   comp
   value
   sdl2-ffi:+sdl-windowpos-undefined+)
  value)

(defun window-top (comp)
  (cffi:with-foreign-objects ((y :int))
    (sdl2-ffi.functions:sdl-get-window-position
     (window comp) (cffi:null-pointer) y)
    (cffi:mem-ref y :int)))

(defun (setf window-top) (value comp)
  (setf value (round value))
  (sdl2-ffi.functions:sdl-set-window-position
   comp
   sdl2-ffi:+sdl-windowpos-undefined+
   value)
  value)

(defun window-size (comp)
  (cffi:with-foreign-objects ((width :int)
                              (height :int))
    (sdl2-ffi.functions:sdl-gl-get-drawable-size (window comp) width height)
    (values (cffi:mem-ref width :int)
            (cffi:mem-ref height :int))))

(defun window-width (comp)
  (multiple-value-bind (width height)
      (window-size comp)
    (declare (ignore height))
    width))

(defun window-height (comp)
  (multiple-value-bind (width height)
      (window-size comp)
    (declare (ignore width))
    height))

(defun window-close (comp)
  (when (slot-boundp comp 'renderer)
    (media:render-destroy (renderer comp))
    (slot-makunbound comp 'renderer))

  (when (slot-boundp comp 'window)
    (sdl2:destroy-window (window comp))
    (slot-makunbound comp 'window)))

#+(and win32 swank)
(defvar *%first-window* t)

(defmethod initialize-instance :after ((comp window)
                                       &key
                                         (title "")
                                         (x :undefined)
                                         (y :undefined)
                                         (width 800)
                                         (height 600)
                                         (visible t)
                                         &allow-other-keys)
  (let* ((sdl-window
          (sdl2:create-window
           :title title
           :x x :y y
           :w width
           :h height
           :flags (list (if visible :shown :hidden) :resizable)))
         (sdl-renderer
          (sdl2:create-renderer
           sdl-window -1 (list :accelerated))))
    (sdl2:set-render-draw-blend-mode sdl-renderer sdl2-ffi:+sdl-blendmode-blend+)

    (setf (slot-value comp 'window) sdl-window
          (slot-value comp 'renderer) (make-instance 'media:renderer :native sdl-renderer))

    #+(and win32 swank)
    (progn
      (when (and visible *%first-window*)
        (sdl2:hide-window sdl-window)
        (sdl2:show-window sdl-window))
      (setf *%first-window* nil)))

  (setf (gethash (sdl2:get-window-id (window comp)) (windows (ui-manager))) comp)

  (event:event-subscribe
   (input:e_window-event (input:input-manager))
   comp
   '%on-window-event))

(defmethod (setf width) ((comp window) val)
  (call-next-method)
  (sdl2:set-window-size (window comp)
                        (width comp)
                        (height comp)))

(defmethod (setf height) ((comp window) val)
  (call-next-method)
  (sdl2:set-window-size (window comp)
                        (width comp)
                        (height comp)))

(defmethod measure-override ((comp window) available-width available-height)
  (let ((des-w (window-width comp))
        (des-h (window-height comp)))
    (when (%presenter comp)
      (multiple-value-bind (min-w max-w min-h max-h)
          (%window-min-max comp)
        (let ((w (or (and available-width (max (min available-width max-w) min-w))
                     max-w))
              (h (or (and available-height (max (min available-height max-h) min-h))
                     max-h)))
          (measure (%presenter comp) w h))))
    (values des-w des-h)))

(defmethod arrange-override ((comp window) width height)
  (multiple-value-bind (min-w max-w min-h max-h)
      (%window-min-max comp)
    (setf width (max (min width max-w) min-w))
    (setf height (max (min height max-h) min-h))

    (when (%presenter comp)
      (arrange (%presenter comp) 0 0 width height)))
  (values width height))

(defmethod draw :around ((comp window))
  (load-component comp)
  (measure comp nil nil)
  (arrange comp 0 0 (desired-width comp) (desired-height comp))
  (call-next-method))

(defun %window-min-max (comp)
  (let (min-w max-w min-h max-h)
    (setf min-w (or (min-width comp) 0))
    (setf min-h (or (min-height comp) 0))
    (setf max-w (or (max-width comp) (window-width comp)))
    (setf max-h (or (max-height comp) (window-height comp)))
    (values min-w max-w min-h max-h)))

(defun %on-window-event (comp args)
  (when (eql (input:window-id args)
             (sdl2:get-window-id (window comp)))

    (case (input:event-type args)
      (:close
       (event:event-unsubscribe
        (input:e_window-event (input:input-manager))
        comp)
       (window-close comp)
       (remhash (sdl2:get-window-id (window comp)) (windows (ui-manager)))
       (event:event-notify
        (e_window-closed comp)
        comp))
      (:focus-gained
       (setf (active-focus-manager)
             (focus-manager comp)))
      (:focus-lost
       (setf (active-focus-manager) nil)))))