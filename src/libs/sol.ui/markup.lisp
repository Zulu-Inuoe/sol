;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.ui)

(defun %load-component-tree (s-exp)
  (cond
    ((atom s-exp) s-exp)
    ((null s-exp) nil)
    ((listp (car s-exp))
     (cons (mapcar #'%load-component-tree (car s-exp))
           (%load-component-tree (cdr s-exp))))
    (t
     (apply 'make-instance (cons (car s-exp) (mapcar #'%load-component-tree (cdr s-exp)))))))

(defun load-component-tree (path)
  "Loads up a component tree from the descriptor in path."
  (with-open-file (file path :direction :input :element-type 'character)
    (%load-component-tree (read file))))