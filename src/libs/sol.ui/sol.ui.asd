;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defsystem #:sol.ui
  :name "sol.ui"
  :version "0.0.0.0"
  :description "UI System for SOL"
  :author "Wilfredo Velázquez-Rodríguez <zulu.inuoe@gmail.com>"
  :license "zlib/libpng License <http://opensource.org/licenses/zlib-license.php>"
  :components
  ((:file "package")

   (:file "command-base" :depends-on ("package"))
   (:file "delegate-command" :depends-on ("command-base"))

   (:file "observable-object" :depends-on ("package"))
   (:file "mouse" :depends-on ("package"))

   (:file "routed-event" :depends-on ("package"))
   (:file "focus-event-args" :depends-on ("routed-event"))
   (:file "focus-manager" :depends-on ("focus-event-args"))
   (:file "input-event-args" :depends-on ("routed-event"))

   (:file "component" :depends-on ("package" "mouse" "routed-event"))

   (:file "image" :depends-on ("component"))
   (:file "text-component" :depends-on ("component"))
   (:file "label" :depends-on ("text-component"))
   (:file "textbox" :depends-on ("text-component"))

   (:file "shapes" :depends-on ("package"))

   (:file "content-presenter" :depends-on ("component" "image" "label"))
   (:file "content-control" :depends-on ("component" "content-presenter"))
   (:file "window" :depends-on ("content-control"))
   (:file "button" :depends-on ("command-base" "content-control"))

   (:file "panel" :depends-on ("component"))
   (:file "stack-panel" :depends-on ("panel"))
   (:file "dock-panel" :depends-on ("panel"))
   (:file "grid" :depends-on ("panel"))

   (:file "ui-manager" :depends-on ("package")))

  :depends-on
  (#:alexandria
   #:closer-mop
   #:sdl2
   #:sol.event
   #:sol.input
   #:sol.media))
