;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:sol.ui)

(defclass ui-manager ()
  ((windows
    :type hash-table
    :initform (make-hash-table)
    :reader windows)))

(defun ui-manager-init ()
  (setf *%ui-manager* (make-instance 'ui-manager))

  (let ((input (input:input-manager)))
    (event:event-subscribe
     (input:e_mouse-button input)
     *%ui-manager*
     '%ui.on-mouse-button)
    (event:event-subscribe
     (input:e_mouse-move input)
     *%ui-manager*
     '%ui.on-mouse-move)
    (event:event-subscribe
     (input:e_mouse-wheel input)
     *%ui-manager*
     '%ui.on-mouse-wheel)
    (event:event-subscribe
     (input:e_key input)
     *%ui-manager*
     '%ui.on-key)
    (event:event-subscribe
     (input:e_text-input input)
     *%ui-manager*
     '%ui.on-text-input)
    (event:event-subscribe
     (input:e_window-event input)
     *%ui-manager*
     '%ui.on-window-event)))

(defun ui-manager ()
  *%ui-manager*)

(defun ui-manager-uninit ()
  (let ((input (input:input-manager)))
    (event:event-unsubscribe
     (input:e_mouse-button input)
     *%ui-manager*)
    (event:event-unsubscribe
     (input:e_mouse-move input)
     *%ui-manager*)
    (event:event-unsubscribe
     (input:e_mouse-wheel input)
     *%ui-manager*)
    (event:event-unsubscribe
     (input:e_key input)
     *%ui-manager*)
    (event:event-unsubscribe
     (input:e_text-input input)
     *%ui-manager*)
    (event:event-unsubscribe
     (input:e_window-event input)
     *%ui-manager*))

  (dolist (w (hash-table-values (windows *%ui-manager*)))
     (window-close w))
  (clrhash (windows *%ui-manager*))
  (setf *%ui-manager* nil))

(defvar *%ui-manager* nil)

(defun %ui.on-mouse-button (ui args)
  (when-let ((window (gethash (input:window-id args) (windows ui))))
    (when-let ((target (or (capturing-component)
                           (and window (get-component-at-* window (input:x args) (input:y args))))))
      (raise-event
       target
       (if (input:button-pressed args)
           'e_mouse-down
           'e_mouse-up)
       (make-instance
        'routed-input-args
        :source target
        :args args)))))

(defun %ui.on-mouse-move (ui args)
  (when-let ((window (gethash (input:window-id args) (windows ui))))
    (when-let ((target (or (capturing-component)
                           (and window (get-component-at-* window (input:x args) (input:y args))))))
      (raise-event
       target
       'e_mouse-move
       (make-instance
        'routed-input-args
        :source target
        :args args))

      (unless (capturing-component)
        (setf (%mouse-over-component) target)))))

(defun %ui.on-mouse-wheel (ui args)
  (when-let ((window (gethash (input:window-id args) (windows ui))))
    (when-let ((target (or (capturing-component)
                           (and window (get-component-at-* window (input:x args) (input:y args))))))
      (raise-event
       target
       'e_mouse-wheel
       (make-instance
        'routed-input-args
        :source target
        :args args)))))

(defun %ui.on-key (ui args)
  (declare (ignore ui))
  (when-let ((target (focused-component)))
    (raise-event
     target
     (if (input:key-pressed args)
         'e_key-down
         'e_key-up)
     (make-instance
      'routed-input-args
      :source target
      :args args))))

(defun %ui.on-text-input (ui args)
  (declare (ignore ui))
  (when-let ((target (focused-component)))
    (raise-event
     target
     'e_text-input
     (make-instance
      'routed-input-args
      :source target
      :args args))))

(defun %ui.on-window-event (ui args)
  (when-let ((window (gethash (input:window-id args) (windows ui))))
    (case (input:event-type args)
      (:enter
       (multiple-value-bind (x y) (input:mouse-position)
         (setf (%mouse-over-component)
               (get-component-at-* window (- x (window-left window)) (- y (window-top window))))))
      (:leave
       (setf (%mouse-over-component) nil)))))